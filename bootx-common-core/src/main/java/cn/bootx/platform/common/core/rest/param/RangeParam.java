package cn.bootx.platform.common.core.rest.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author duriea
 */
@Data
@Schema(title = "时间范围查询")
public class RangeParam {

    @Schema(description = "开始时间")
    private LocalDateTime startTime;

    @Schema(description = "结束时间")
    private LocalDateTime endTime;

}
