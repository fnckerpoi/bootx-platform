package cn.bootx.platform.visualization.param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalDate;

import cn.bootx.platform.common.core.annotation.QueryParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 计划完成进度表，关注某产品每批累计生产进度，每个品种产品，以月自然日1-30号生产记录，有排产数量或完工数量的，填入对应的品种日期，无的，填0
 * @author luokanran
 * @since 2023-09-18
 */
@QueryParam(type = QueryParam.CompareTypeEnum.LIKE)
@Data
@Schema(title = "计划完成进度表，关注某产品每批累计生产进度，每个品种产品，以月自然日1-30号生产记录，有排产数量或完工数量的，填入对应的品种日期，无的，填0")
@Accessors(chain = true)
public class RptPlanexecuteByproductParam {

    @Schema(description= "主键")
    private Long id;

    @Schema(description = "主键")
    private Long fid;
    @Schema(description = "创建时间")
    private LocalDateTime fcreatedatetime;
    @Schema(description = "创建人；取登录用户")
    private String fcreateman;
    @Schema(description = "最后修改时间")
    private LocalDateTime flastmodifydatetime;
    @Schema(description = "最后修改人；取用户名")
    private String flastmodifyman;
    @Schema(description = "自然日期，1-30号生成")
    private LocalDateTime fbizdatetime;
    @Schema(description = "产品名称，取协同平台产品名")
    private String fmaterialname;
    @Schema(description = "产品编码，取协同平台产品号")
    private String fmaterialcode;
    @Schema(description = "计划数量，取自APS导到协同平台数据")
    private Integer fapsqty;
    @Schema(description = "计划数量本批累，将同一批的 fapsqty累加")
    private Integer fapsqtyAccumulation;
    @Schema(description = "扫码数量，取自追溯后台")
    private Integer fproductqty;
    @Schema(description = "扫码数量本批累，将同一批的 fproductqty累加")
    private Integer fproductqtyAccumulation;
    @Schema(description = "公司ID，关联协同平台  t_org_company")
    private Long fcompanyid;
    @Schema(description = "")
    private String fcompanyname;

}
