package cn.bootx.platform.visualization.core.dao;

import cn.bootx.platform.common.mybatisplus.impl.BaseManager;

import cn.bootx.platform.visualization.core.dao.MapperImpl.DeviceTracksMapper;
import cn.bootx.platform.visualization.core.entity.DeviceTracks;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Repository
@RequiredArgsConstructor
public class DeviceTracksManager extends BaseManager<DeviceTracksMapper, DeviceTracks> {

    @DS("#dateSource")
    public List<DeviceTracks> selectByMainIds(String dateSource, List<Long> deviceIds) {
        return lambdaQuery().in(DeviceTracks::getDeviceId,deviceIds).list();
    }
}
