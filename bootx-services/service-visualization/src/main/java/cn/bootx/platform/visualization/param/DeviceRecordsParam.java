package cn.bootx.platform.visualization.param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalDate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * @author xxm
 * @date 2023-05-18
 */
@Data
@Schema(title = "")
@Accessors(chain = true)
public class DeviceRecordsParam {

    @Schema(description= "主键")
    private Long id;

    @Schema(description = "")
    private String name;
    @Schema(description = "")
    private String description;
    @Schema(description = "")
    private Integer categoryId;
    @Schema(description = "")
    private Integer vendorId;
    @Schema(description = "")
    private String mac;
    @Schema(description = "")
    private String ip;
    @Schema(description = "")
    private String photo;
    @Schema(description = "")
    private Double price;
    @Schema(description = "")
    private LocalDate purchased;
    @Schema(description = "")
    private LocalDate expired;
    @Schema(description = "")
    private Integer depreciationRuleId;
    @Schema(description = "")
    private String assetNumber;
    @Schema(description = "")
    private LocalDateTime deletedAt;
    @Schema(description = "")
    private LocalDateTime createdAt;
    @Schema(description = "")
    private LocalDateTime updatedAt;
    @Schema(description = "")
    private LocalDateTime discardAt;
//    @Schema(description = "")
//    private String location;
//    @Schema(description = "")
//    private String specification;

}