package cn.bootx.platform.visualization.code;

import java.util.Arrays;
import java.util.List;

/**
 * GoVIew大屏相关常量
 *
 * @author xxm
 * @since 2023/2/21
 */
public interface GoVIewCode {

    /** 发布 */
    int STATE_PUBLISH = 1;

    /** 未发布 */
    int STATE_UN_PUBLISH = -1;

    List<Integer> COMPUTER_CATEGORY = Arrays.asList(1,2,10);

    List<String> CHEMEX_LIST = Arrays.asList("chemex@zs","chemex@yz","chemex@hb");

}
