package cn.bootx.platform.visualization.core.service;

import cn.bootx.platform.common.core.exception.DataNotExistException;
import cn.bootx.platform.common.core.rest.param.RangeParam;
import cn.bootx.platform.visualization.core.dao.RptTotalDayManager;
import cn.bootx.platform.visualization.core.entity.RptTotalDay;
import cn.bootx.platform.visualization.dto.RptTotalDayDto;
import cn.bootx.platform.visualization.param.RptTotalDayParam;
import cn.bootx.platform.visualization.param.enumParam.PerfectBIScreeEnum;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static cn.bootx.platform.visualization.param.enumParam.PerfectBIScreeEnum.*;

import static java.util.stream.Collectors.groupingBy;


/**
 * MES类-按日统计的数据
 *
 * @author Duriea
 * @date 2023-05-15
 */
@Slf4j
@Service
@RequiredArgsConstructor
@DS("bigScreen")
public class RptTotalDayService {
    private final RptTotalDayManager rptTotalDayManager;

    /**
     * 按中心级统计
     *
     * @param pageParam
     * @param rangeParam
     * @param baseLevel
     * @return
     */
    public Map<String, Object> biScreenByCenter(RptTotalDayParam pageParam, RangeParam rangeParam, Integer baseLevel) {
        List<RptTotalDay> list = Optional.ofNullable(rptTotalDayManager.selectByCenter(pageParam, rangeParam, baseLevel))
                .orElseThrow(DataNotExistException::new)
                .stream().sorted(Comparator.comparing(RptTotalDay::getFdate))
                .collect(Collectors.toList());

        Map<Integer, List<RptTotalDay>> map = list.stream()
                .collect(groupingBy(RptTotalDay::getFtypeid));

        //通过反射机制，用不同的key去执行对应的业务方法封装。
        Map<String, Object> result = new HashMap<>();
        for (Map.Entry<Integer, List<RptTotalDay>> eachGroup : map.entrySet()) {
            try {
                String methodName = Objects.requireNonNull(
                        PerfectBIScreeEnum.PerfectBIScreenDataTypeEnumOfCenter.getEnum(eachGroup.getKey())
                ).name();

                result.put(methodName,
                        ReflectUtil.invoke(this, methodName, eachGroup.getValue()));

            } catch (Exception e) {
                log.info("biScreenByCenter业务缺乏枚举类型或者处理函数。数据类型{}", eachGroup.getKey());
//                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 本日新增排程数量\月度排产笔数趋势\月度排产产品数量对比
     *
     * @param dataList
     * @return
     */
    public Map<Object, Object> newSchedulesAddedPerDay(List<RptTotalDay> dataList) {
        //本日新增排程数量
        RptTotalDay todayOrYesterday = dataList.stream()
                .max(Comparator.comparing(RptTotalDay::getFdate))
                .orElseThrow(DataNotExistException::new);

        return new HashMap<Object, Object>() {
            {
                put("toDayNewSchedules", todayOrYesterday.getFqty());
                put("monthlySchedules", monthlySchedulesResult(dataList));
//                put("comparMonthlySchedules", comparMonthlySchedules(dataList));
            }
        };
    }

    //月度排产笔数趋势
    private Object monthlySchedulesResult(List<RptTotalDay> dataList) {
        return histogramGeneral(dataList);
    }

    //月度排产产品数量对比
    private Object comparMonthlySchedules(List<RptTotalDay> dataList) {
        Map<Object, Object> result = new HashMap<>();
        TreeSet<String> titleList = new TreeSet<>(Collections.singletonList("xDir"));

        List<Object> source = new ArrayList<>();
        //先按月分组
        TreeMap<Integer, List<RptTotalDay>> theSameMonth = dataList
                .stream().collect(groupingBy(
                        a -> Integer.parseInt(DateUtil.format(a.getFdate(), "MM")),
                        TreeMap::new,
                        Collectors.toList()));

        //再按年分组，先从小年遍历开始
        for (Map.Entry<Integer, List<RptTotalDay>> month : theSameMonth.entrySet()) {
            TreeMap<Integer, List<RptTotalDay>> theSameYear = month.getValue()
                    .stream().collect(groupingBy(
                                    a -> Integer.parseInt(DateUtil.format(a.getFdate(), "yyyy")),
                                    TreeMap::new,
                                    Collectors.toList()
                            )
                    );

            Map<Object, Object> detailRow = new HashMap<>();
            //读取今年、去年的年月
            for (Map.Entry<Integer, List<RptTotalDay>> eachYearMonth : theSameYear.entrySet()) {
                BigDecimal sum = new BigDecimal(0);
                TreeSet<String> xDirSet = new TreeSet<>();
                for (RptTotalDay qty : eachYearMonth.getValue()) {
                    xDirSet.add(DateUtil.format(qty.getFdate(), DatePattern.NORM_MONTH_PATTERN));
                    sum = sum.add(qty.getFqty());
                    detailRow.put(eachYearMonth.getKey(), sum);
                }
                StringBuilder xDir = new StringBuilder();
                for (String xAxi : xDirSet) {
                    xDir.append("/").append(xAxi);
                }
                detailRow.put("xDir", xDir.replace(0, 1, ""));
                titleList.add(String.valueOf(eachYearMonth.getKey()));
            }
            source.add(detailRow);
        }
        result.put("dimensions", titleList.descendingSet());
        result.put("source", source);
        return result;
    }

    /**
     * 预喷任务数量
     *
     * @param dataList
     * @return
     */
    public BigDecimal numOfPreSpray(List<RptTotalDay> dataList) {
        RptTotalDay todayOrYesterday = dataList.stream().max(Comparator.comparing(RptTotalDay::getFdate)).orElseThrow(DataNotExistException::new);
        return todayOrYesterday.getFqty();
    }

    /**
     * 无批号生产记录
     *
     * @param dataList
     * @return
     */
    public BigDecimal notBatchProductionRecord(List<RptTotalDay> dataList) {
        RptTotalDay todayOrYesterday = dataList.stream()
                .max(Comparator.comparing(RptTotalDay::getFdate))
                .orElseThrow(DataNotExistException::new);
        return todayOrYesterday.getFqty();
    }

    /**
     * 半成品请检数
     *
     * @param dataList
     * @return
     */
    public BigDecimal pleaseCheckTheNumberOfSemiFinishedProducts(List<RptTotalDay> dataList) {
        RptTotalDay todayOrYesterday = dataList.stream()
                .max(Comparator.comparing(RptTotalDay::getFdate))
                .orElseThrow(DataNotExistException::new);
        return todayOrYesterday.getFqty();
    }

    /**
     * 月度OEM/ODM
     *
     * @param dataList
     * @return
     */
    public Object monthlyOEemByOdm(List<RptTotalDay> dataList) {
        return histogramGeneral(dataList);
    }


    /**
     * 中心级总产量
     * @param dataList
     * @return
     */
    public Object centerOutput(List<RptTotalDay> dataList){
        return histogramGeneral(dataList);
    }

    public Object oemOdmOrderQty(List<RptTotalDay> dataList){
        return histogramGeneral(dataList);
    }



    /**
     * MES工单近30天统计
     *
     * @param rangeParam
     * @param baseLevel
     * @return
     */
    public Map<String, Object> statisticsRptTotalDay(RangeParam rangeParam, Integer baseLevel) {
        TreeMap<String, TreeMap<Integer, List<RptTotalDayDto>>> groupByDate = rptTotalDayManager.statisticsRptTotalDay(rangeParam, baseLevel)
                .stream().collect(
                        groupingBy(a -> DateUtil.format(a.getFdate(), DatePattern.NORM_DATE_PATTERN)
                                , TreeMap::new,
                                groupingBy(RptTotalDayDto::getFtypeid, TreeMap::new, Collectors.toList())));

        List<TreeMap<String, Object>> horizontalColumnar = new ArrayList<>();
        List<TreeMap<String, Object>> lineChart = new ArrayList<>();
        groupByDate.forEach((k1, v1) -> {
            TreeMap<String, Object> horizontalColumnarChild = new TreeMap<>();
            TreeMap<String, Object> lineChartChild = new TreeMap<>();
            horizontalColumnarChild.put("xDir", k1);
            lineChartChild.put("xDir", k1);
            v1.forEach((k2, v2) -> {
                BigDecimal sum = new BigDecimal(0);
                for (RptTotalDayDto qty : v2) {
                    sum = sum.add(qty.getFqty());
                }
                log.info(String.valueOf(sum));
                horizontalColumnarChild.put(MesWorkOrderTypeEnum.getValue(k2), sum);

                if ((k2.equals(MesWorkOrderTypeEnum.endOfProduction.getKey()))
                        || k2.equals(MesWorkOrderTypeEnum.reviewed.getKey())
                        || k2.equals(MesWorkOrderTypeEnum.noReviewRequired.getKey())
                ) {
                    lineChartChild.put(MesWorkOrderTypeEnum.getValue(k2), sum);
                }
            });
            horizontalColumnar.add(horizontalColumnarChild);

            BigDecimal reviewedSum = (BigDecimal) lineChartChild.getOrDefault(MesWorkOrderTypeEnum.reviewed.getValue(), new BigDecimal(0));
            BigDecimal noReviewRequiredSum = (BigDecimal) lineChartChild.getOrDefault(MesWorkOrderTypeEnum.noReviewRequired.getValue(), new BigDecimal(0));
            lineChartChild.put("审核完成单数", reviewedSum.add(noReviewRequiredSum));
            lineChartChild.remove(MesWorkOrderTypeEnum.reviewed.getValue());
            lineChartChild.remove(MesWorkOrderTypeEnum.noReviewRequired.getValue());
            lineChart.add(lineChartChild);
        });


        Map<String, Object> finalResult = new HashMap<>();

        Map<String, Object> horizontalColumnarResult = new HashMap<>();
        horizontalColumnarResult.put("dimensions", horizontalColumnar.stream().flatMap(a -> a.keySet().stream()).distinct().collect(Collectors.toList()));
        horizontalColumnarResult.put("source", horizontalColumnar);
        finalResult.put("horizontalColumnarChild", horizontalColumnarResult);

        Map<String, Object> lineChartResult = new HashMap<>();
        lineChartResult.put("dimensions", lineChart.stream().flatMap(a -> a.keySet().stream()).distinct().collect(Collectors.toList()));
        lineChartResult.put("source", lineChart);
        finalResult.put("lineChart", lineChartResult);

        return finalResult;
    }

    /**
     * 基地级数据==>汇总为中心级数据；
     *
     * @param rangeParam
     * @return
     */
    public Object shopFloorStatistics(RangeParam rangeParam, Integer baseLevel) {
        Map<String, Object> result = new HashMap<>();

        Map<Integer, List<RptTotalDay>> groupByType = rptTotalDayManager.selectBaseData(rangeParam, baseLevel)
                .stream().collect(groupingBy(RptTotalDay::getFtypeid));

        groupByType.forEach((k, v) -> {
            result.put(
                    Objects.requireNonNull(PerfectBIScreenDataTypeEnumOfBase.getEnum(k)).name(),
                    this.baseSummary(k, v));
        });

        if (result.containsKey(PerfectBIScreenDataTypeEnumOfBase.bigClearance.name()) && result.containsKey(PerfectBIScreenDataTypeEnumOfBase.smallClearance.name())) {
            result.put(
                    "clearance",
                    jsonGeneralProcessing(
                            result.get(PerfectBIScreenDataTypeEnumOfBase.bigClearance.name()),
                            result.get(PerfectBIScreenDataTypeEnumOfBase.smallClearance.name())
                    )
            );
        }

        if (result.containsKey(PerfectBIScreenDataTypeEnumOfBase.electronicScaleNum.name()) && result.containsKey(PerfectBIScreenDataTypeEnumOfBase.electronicScaleValidExpiration.name())) {
            result.put(
                    "electronic",
                    jsonGeneralProcessing(
                            result.get(PerfectBIScreenDataTypeEnumOfBase.electronicScaleNum.name()),
                            result.get(PerfectBIScreenDataTypeEnumOfBase.electronicScaleValidExpiration.name())
                    )
            );
        }
        return result;
    }

    private Object baseSummary(Integer key, List<RptTotalDay> dataList) {
        TreeMap<String, Object> result = new TreeMap<>();
        TreeMap<String, List<RptTotalDay>> groupByDate = dataList.stream().sorted(Comparator.comparing(RptTotalDay::getFdate))
                .collect(groupingBy(a -> DateUtil.format(a.getFdate(), DatePattern.NORM_DATE_PATTERN), TreeMap::new, Collectors.toList()));

        List<LinkedHashMap<String, Object>> source = new ArrayList<>();
        groupByDate.forEach((k, v) -> {
            List<BigDecimal> list = v.stream().map(RptTotalDay::getFqty).collect(Collectors.toList());
            if (list.size() < 1) {
                return;
            }

            double sum = list.stream().mapToInt(BigDecimal::intValue).sum();
            LinkedHashMap<String, Object> row = new LinkedHashMap<>();
            row.put("xDir", k);
            row.put(Objects.requireNonNull(PerfectBIScreenDataTypeEnumOfBase.getEnum(key)).getValue(), sum);
            source.add(row);
        });

        result.put("source", source);
        result.put("dimensions", source.stream().flatMap(a -> a.keySet().stream()).distinct().collect(Collectors.toList()));
        return result;
    }

    /**
     * 基地生产曲线对比图处理
     *
     * @param rangeParam
     * @return
     */
    public Object baseProductionData(RangeParam rangeParam) {
        Map<Integer, TreeMap<String, List<RptTotalDay>>> groupByTypeByFarea = rptTotalDayManager.selectBaseData(rangeParam, null)
                .stream().collect(
                        groupingBy(
                                RptTotalDay::getFtypeid,
                                groupingBy(
                                        a -> DateUtil.format(a.getFdate(), DatePattern.NORM_DATE_PATTERN),
                                        TreeMap::new,
                                        Collectors.toList()
                                )
                        )
                );

        Map<String, Object> result = new HashMap<>();
        for (Map.Entry<Integer, TreeMap<String, List<RptTotalDay>>> dataTypeMap : groupByTypeByFarea.entrySet()) {
            TreeSet<String> dimensions = new TreeSet<>();
            List<LinkedHashMap<String, Object>> source = new ArrayList<>();
            for (Map.Entry<String, List<RptTotalDay>> eachDateEntity : dataTypeMap.getValue().entrySet()) {
                LinkedHashMap<String, Object> row = new LinkedHashMap<>();
                eachDateEntity.getValue().forEach(obj -> {
                    row.put(PerfectBaseName.getValue(obj.getFareaid()), obj.getFqty());
                });
                row.put("xDir", eachDateEntity.getKey());
                source.add(row);
                dimensions.addAll(row.keySet());
            }
            Map<String, Object> eachResult = new HashMap<>();
            eachResult.put("dimensions", dimensions);
            eachResult.put("source", source);
            result.put(PerfectBIScreenDataTypeEnumOfBase.getEnum(dataTypeMap.getKey()).name(), eachResult);
        }
        return result;
    }

    private Object histogramGeneral(List<RptTotalDay> dataList){
        TreeMap<String, Object> result = new TreeMap<>();
        List<Map<String, Object>> source = dataList.stream()
                .map(row -> {
                    Map<String, Object> map = new HashMap<>();
                    map.put("xDir", DateUtil.format(row.getFdate(), DatePattern.NORM_DATE_PATTERN));
                    map.put(DateUtil.format(row.getFdate(), DatePattern.NORM_MONTH_PATTERN), row.getFqty());
                    return map;
                }).collect(Collectors.toList());
        result.put("dimensions", source.stream().flatMap(a -> a.keySet().stream()).distinct().collect(Collectors.toList()));
        result.put("source", source);
        return result;
    }

    /**
     * 柱图、折线对比图JSON通用处理
     * 将同一个日期的数据汇总
     *
     * @param srcDatas
     * @return
     */
    public Object jsonGeneralProcessing(Object... srcDatas) {
        List<JSONArray> arrays = new ArrayList<>();
        for (Object srcData : srcDatas) {
            JSONObject obj = Convert.convert(JSONObject.class, srcData);
            arrays.add(obj.getJSONArray("source"));
        }

        JSONArray sourceArray = new JSONArray();
        LinkedHashSet<String> dimensions = new LinkedHashSet<>();
        for (JSONArray array : arrays) {
            for (int i = 0; i < array.size(); i++) {
                JSONObject item = array.getJSONObject(i);
                String xDir = item.getStr("xDir");
                JSONObject mergedItem = findOrCreateItem(sourceArray, xDir);
                mergedItem.putAll(item);
                dimensions.addAll(item.keySet());
            }
        }
        Map<String, Object> result = new HashMap<>();
        result.put("source", sourceArray);
        result.put("dimensions", dimensions);
        return result;
    }

    private static JSONObject findOrCreateItem(JSONArray array, String xDir) {
        for (int i = 0; i < array.size(); i++) {
            JSONObject item = array.getJSONObject(i);
            if (xDir.equals(item.getStr("xDir"))) {
                return item;
            }
        }
        JSONObject newItem = new JSONObject();
        newItem.append("xDir", xDir);
        array.add(newItem);
        return newItem;
    }


}
