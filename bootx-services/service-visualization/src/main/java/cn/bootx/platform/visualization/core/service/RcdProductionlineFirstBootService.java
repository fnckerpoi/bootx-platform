package cn.bootx.platform.visualization.core.service;

import cn.bootx.platform.visualization.core.dao.RcdProductionlineFirstBootManager;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineBootCountParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineBootReportParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineFirstBootRankParam;
import cn.bootx.platform.visualization.vo.ProductionLineBootReportVO;
import cn.bootx.platform.visualization.vo.ProductionLineFirstBootRankVO;
import cn.hutool.core.date.DateUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Service
@AllArgsConstructor
@DS("bigScreen")
public class RcdProductionlineFirstBootService {

    private final RcdProductionlineFirstBootManager rcdProductionlineFirstBootManager;

    public Map<String,Object> getProductionLineFirstBootRank(GetProductionLineFirstBootRankParam param){

        List<ProductionLineFirstBootRankVO> productionLineFirstBootRankList = rcdProductionlineFirstBootManager.getProductionLineFirstBootRankList(param);

        List<Map<String,Object>> maps = new ArrayList<>();

        productionLineFirstBootRankList.forEach(i -> {
            Map<String,Object> map = new HashMap<>();

            String name = replaceTemplate(param.getTemplate(),i.getCompanyName(),i.getFactoryName(),i.getWorkshopName(),i.getProductionlineName());

            map.put("name",name);
            map.put("开机时间",DateUtil.format(i.getFfirstbootdatetime(),"yyyy-MM-dd HH:mm:ss"));
            maps.add(map);
        });
        return new HashMap<String,Object>(){{
            put("dimensions",new ArrayList<String>(){{
                add("name");
                add("开机时间");
            }});
            put("source",maps);
        }};
    }

    public Map<String,Object> getProductionLineBootReport(GetProductionLineBootReportParam param) {

        List<ProductionLineBootReportVO> productionLineBootReportList = rcdProductionlineFirstBootManager.getProductionLineBootReportList(param);

        Map<String,Object> map = new HashMap<>();
        List<String> dimensions = new ArrayList<>();
        List<Map<String,Object>> source = new ArrayList<>();
        map.put("dimensions",dimensions);
        map.put("source",source);
        dimensions.add("day");
        if (CollectionUtils.isEmpty(productionLineBootReportList) && param.getType() == 1) {
            // 根据产线编号查出产线信息
            ProductionLineBootReportVO productionLineBootReportVO = rcdProductionlineFirstBootManager.getProductionLineInfoByFnumber(param.getSearchCode());
            String name = replaceTemplate(param.getTemplate(),
                    productionLineBootReportVO.getCompanyName(),
                    productionLineBootReportVO.getFactoryName(),
                    productionLineBootReportVO.getWorkshopName(),
                    productionLineBootReportVO.getProductionlineName());
            dimensions.add(name);
        }


//        Date currentDate = DateUtil.beginOfDay(new Date());
//
//        long dayTimestamp = 1000 * 60 * 60 * 24;

        TreeMap<String,Map<String,Object>> cache = new TreeMap<>();

        AtomicReference<Date> minDate = new AtomicReference<>(new Date(Long.MAX_VALUE));

        AtomicReference<Date> specialMinDate = new AtomicReference<>(new Date(Long.MAX_VALUE));

        productionLineBootReportList.forEach(item -> {
            String name = replaceTemplate(param.getTemplate(),
                    item.getCompanyName(),
                    item.getFactoryName(),
                    item.getWorkshopName(),
                    item.getProductionlineName());
            dimensions.add(name);
            String[] split = item.getDateStr().split(",");
            for (String d : split) {
                Date mDate = DateUtil.parse(d);
                if (mDate.getTime() < minDate.get().getTime()) minDate.set(mDate);
                // 特殊格式仅仅比较时分秒不比较年月日
                long mDateHMS = mDate.getTime() - DateUtil.beginOfDay(mDate).getTime();
                long specialHMS = specialMinDate.get().getTime() - DateUtil.beginOfDay(specialMinDate.get()).getTime();
                if (mDateHMS < specialHMS) specialMinDate.set(mDate);


                if (StringUtils.isBlank(d)) continue;
                // 取出日期
                String substring = d.substring(0, 10);

                // 判断cache中是否存在
                if (cache.containsKey(substring)){
                    cache.get(substring).put(name,d);
                }else {
                    cache.put(substring,new HashMap<String,Object>(){{
                        put(name,d);
                    }});
                }
            }
        });



        Date date = minDate.get();

        String minDateStr = DateUtil.format(date,"yyyy-MM-dd HH:mm:ss");
        // 遍历
        cache.forEach((k,v) -> source.add(new HashMap<String,Object>(){{
            put("day",k);
            // 把v map里的value日期换成同一个时间日期
            if (param.getToSpecial()){
                Map<String,Object> newMap = new HashMap<>();
                v.forEach((k1,v1) -> {
                    String vDateStr = v1.toString();
                    String minDate1Str = minDateStr.substring(0,10);
                    String value = minDate1Str + " " + vDateStr.substring(11);
                    newMap.put(k1,value);
                });
                putAll(newMap);
            }else {
                putAll(v);
            }
        }}));

        map.put("cache",cache);

        map.put("seriesType",param.getSeriesType());


        // 判断是否是特殊格式
        long hourMill = 1000 * 60 * 60;
        if (param.getToSpecial()){
            // 是特殊格式，需要将日期转化为最小日期那一天
            Date spDate = specialMinDate.get();

            long specialHMS = spDate.getTime() - DateUtil.beginOfDay(spDate).getTime();

            spDate = new Date(DateUtil.beginOfDay(date).getTime() + specialHMS);


            // 判断是否是减掉一小时之后是否还是当天，如果不是则不减
            long temp1 = DateUtil.beginOfDay(spDate).getTime();
            long temp2 = DateUtil.beginOfDay(new Date(spDate.getTime() - hourMill)).getTime();

            if (temp1 == temp2){
                spDate = new Date(spDate.getTime() - hourMill);
            }

            map.put("minDate",DateUtil.format(spDate,"yyyy-MM-dd HH:mm:ss"));
        }else {
            map.put("minDate", minDateStr);
        }

        return map;
    }

    public Integer getProductionLineBootCount(GetProductionLineBootCountParam param) {
        return rcdProductionlineFirstBootManager.getProductionLineBootCount(param);
    }

    private static String replaceTemplate(String template,String companyName,String factoryName,String workshopName,String productionlineName){
        Map<String,String> templateMap = new HashMap<String,String>(){
            {
                put("company",companyName);
                put("factory",factoryName);
                put("workshop",workshopName);
                put("productionline",productionlineName);
            }
        };
        for (String key : templateMap.keySet()) {
            if (template.contains(key)) template = template.replace(key,templateMap.get(key));
        }
        return template;
    }



}
