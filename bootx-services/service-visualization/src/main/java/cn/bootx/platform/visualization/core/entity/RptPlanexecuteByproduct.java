package cn.bootx.platform.visualization.core.entity;

import java.time.LocalDateTime;

import cn.bootx.platform.common.core.function.EntityBaseFunction;

import cn.bootx.platform.visualization.core.convert.RptPlanexecuteByproductConvert;
import cn.bootx.platform.visualization.dto.RptPlanexecuteByproductDto;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.bootx.platform.visualization.param.RptPlanexecuteByproductParam;

import lombok.Data;
import lombok.experimental.Accessors;

/**
* 计划完成进度表，关注某产品每批累计生产进度，每个品种产品，以月自然日1-30号生产记录，有排产数量或完工数量的，填入对应的品种日期，无的，填0
* @author luokanran
* @since 2023-09-18
*/
@Data
@Accessors(chain = true)
@TableName("t_rpt_planexecute_byproduct")
public class RptPlanexecuteByproduct  implements EntityBaseFunction<RptPlanexecuteByproductDto>{

    /** 主键 */
    private Long fid;
    /** 创建时间 */
    private LocalDateTime fcreatedatetime;
    /** 创建人；取登录用户 */
    private String fcreateman;
    /** 最后修改时间 */
    private LocalDateTime flastmodifydatetime;
    /** 最后修改人；取用户名 */
    private String flastmodifyman;
    /** 自然日期，1-30号生成 */
    private LocalDateTime fbizdatetime;
    /** 产品名称，取协同平台产品名 */
    private String fmaterialname;
    /** 产品编码，取协同平台产品号 */
    private String fmaterialcode;
    /** 计划数量，取自APS导到协同平台数据 */
    private Integer fapsqty;
    /** 计划数量本批累，将同一批的 fapsqty累加 */
    private Integer fapsqtyAccumulation;
    /** 扫码数量，取自追溯后台 */
    private Integer fproductqty;
    /** 扫码数量本批累，将同一批的 fproductqty累加 */
    private Integer fproductqtyAccumulation;
    /** 公司ID，关联协同平台  t_org_company */
    private Long fcompanyid;
    /**  */
    private String fcompanyname;

    /** 创建对象 */
    public static RptPlanexecuteByproduct init(RptPlanexecuteByproductParam in) {
            return RptPlanexecuteByproductConvert.CONVERT.convert(in);
    }

    /** 转换成dto */
    @Override
    public RptPlanexecuteByproductDto toDto() {
        return RptPlanexecuteByproductConvert.CONVERT.convert(this);
    }
}
