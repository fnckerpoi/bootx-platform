package cn.bootx.platform.visualization.core.service;

import cn.bootx.platform.common.core.util.ResultConvertUtil;
import cn.bootx.platform.visualization.code.GoVIewCode;
import cn.bootx.platform.visualization.core.dao.DeviceTracksManager;
import cn.bootx.platform.visualization.core.entity.DeviceTracks;
import cn.bootx.platform.visualization.dto.DeviceTracksDto;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DeviceTracksService {
    private final DeviceTracksManager deviceTracksManager;

    /**
     * 获取全部
     */
    public List<DeviceTracksDto> findAll(){
        return ResultConvertUtil.dtoListConvert(deviceTracksManager.findAll());
    }

    public List<DeviceTracksDto> selectByMainIds(List<Long> deviceIds, Integer baseLevel){
        List<DeviceTracks> result = new ArrayList<>();
        if (ObjectUtil.isEmpty(baseLevel)){
            GoVIewCode.CHEMEX_LIST.forEach(dateSource->{
                result.addAll(deviceTracksManager.selectByMainIds(dateSource,deviceIds));
            });
        }else{
            result.addAll(deviceTracksManager.selectByMainIds(GoVIewCode.CHEMEX_LIST.get(baseLevel-1),deviceIds ));
        }
        return ResultConvertUtil.dtoListConvert(result);
    }

}
