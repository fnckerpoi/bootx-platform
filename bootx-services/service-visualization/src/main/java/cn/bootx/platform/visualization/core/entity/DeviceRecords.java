package cn.bootx.platform.visualization.core.entity;

import cn.bootx.platform.common.core.function.EntityBaseFunction;
import cn.bootx.platform.common.mybatisplus.base.MpIdEntity;
import cn.bootx.platform.visualization.core.convert.DeviceRecordsConvert;
import cn.bootx.platform.visualization.dto.DeviceRecordsDto;
import cn.bootx.platform.visualization.param.DeviceRecordsParam;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* 
* @author xxm
* @date 2023-05-18
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName("device_records")
public class DeviceRecords extends MpIdEntity implements EntityBaseFunction<DeviceRecordsDto> {

    /**  */
    private String name;
    /**  */
    private String description;
    /**  */
    private Integer categoryId;
    /**  */
    private Integer vendorId;
    /**  */
    private String mac;
    /**  */
    private String ip;
    /**  */
    private String photo;
    /**  */
    private Double price;
    /**  */
    private LocalDate purchased;
    /**  */
    private LocalDate expired;
    /**  */
    private Integer depreciationRuleId;
    /**  */
    private String assetNumber;
    /**  */
    private LocalDateTime deletedAt;
    /**  */
    private LocalDateTime createdAt;
    /**  */
    private LocalDateTime updatedAt;
    /**  */
    private LocalDateTime discardAt;

    /** 创建对象 */
    public static DeviceRecords init(DeviceRecordsParam in) {
            return DeviceRecordsConvert.CONVERT.convert(in);
    }

    /** 转换成dto */
    @Override
    public DeviceRecordsDto toDto() {
        return DeviceRecordsConvert.CONVERT.convert(this);
    }
}
