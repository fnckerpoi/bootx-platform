package cn.bootx.platform.visualization.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.io.Serializable;

/**
 * 产线每日运行分钟数。未运行也需要有记录，fminute=0(TRcdRunminute)实体类
 *
 * @author makejava
 * @since 2024-02-05 13:49:27
 */

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_rcd_runminute")
public class TRcdRunminute {
/**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private Long fid;
/**
     * 创建时间
     */
    private Date fcreatedatetime;
/**
     * 创建人；取登录用户
     */
    private String fcreateman;
/**
     * 最后修改时间
     */
    private Date flastmodifydatetime;
/**
     * 最后修改人；取用户名
     */
    private String flastmodifyman;
/**
     * 产线ID，关联 t_org_productionline
     */
    private Long fproductionlineid;
/**
     * 冗余存储，取自 t_org_productionline:fnumber
     */
    private String fproductioncode;
/**
     * 统计日期
     */
    private Date fdate;
/**
     * 运行的分钟数
     */
    private Integer fminute;

}

