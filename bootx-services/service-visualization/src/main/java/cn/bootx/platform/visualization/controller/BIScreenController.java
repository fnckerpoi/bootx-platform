package cn.bootx.platform.visualization.controller;

import cn.bootx.platform.common.core.rest.Res;
import cn.bootx.platform.common.core.rest.ResResult;
import cn.bootx.platform.common.core.rest.param.RangeParam;
import cn.bootx.platform.common.core.util.LocalDateTimeUtil;
import cn.bootx.platform.visualization.core.entity.RptPlanexecuteByproduct;
import cn.bootx.platform.visualization.core.service.*;
import cn.bootx.platform.visualization.param.RptTotalDayParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineBootCountParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineBootReportParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineFirstBootRankParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineRunMinuteReportParam;
import cn.hutool.core.date.DateUtil;
import com.alibaba.excel.util.DateUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 完美大屏看板接口
 *
 * @author Duriea
 * @date 2023-05-15
 */
@Tag(name = "完美大屏看板接口")
@RestController
@RequestMapping("/biScreen")
@RequiredArgsConstructor
public class BIScreenController {
    private final RptTotalDayService rptTotalDayService;
    private final DeviceRecordsService deviceRecordsService;
    private final SoftwareRecordsService softwareRecordsService;
    private final AcEntrytotalService acEntrytotalService;
    private final RptPlanexecuteByproductService byproductService;
    private final RcdProductionlineFirstBootService rcdProductionlineFirstBootService;
    private final TRcdRunminuteService rcdRunminuteService;

    @Operation(summary = "APS产品收集公共接口，处理各类产品的排产、生产累计曲线图")
    @GetMapping(value = "/planexecuteByProduct")
    public ResResult<?> planexecuteByProduct(
            RptPlanexecuteByproduct pageParam,
            RangeParam rangeParam,
            boolean isHideFprqty
    ) {
        rangeParam.setEndTime(DateUtil.toLocalDateTime(DateUtil.endOfMonth(DateUtil.nextMonth().toCalendar())));
        rangeParam.setStartTime(DateUtil.toLocalDateTime(DateUtil.beginOfMonth((DateUtil.lastMonth().toCalendar()))));
        return Res.ok(byproductService.categoryOfEachProduct(pageParam,rangeParam,isHideFprqty));
    }

    @Operation(summary = "APS产品收集公共接口，查询每款产品的产量")
    @GetMapping(value = "/getEachProductYield")
    public ResResult<?> getEachProductYield(
    ) {
        return Res.ok(byproductService.yieldOfEachProduct(DateUtil.format(LocalDateTime.now(), DateUtils.DATE_FORMAT_10)));
    }


    @Operation(summary = "排产收集公共接口，按时间范围查询")
    @GetMapping(value = "/biScreenByTimeRange")
    public ResResult<?> biScreenByTimeRange(
            RptTotalDayParam pageParam,
            RangeParam rangeParam,
            Integer baseLevel
    ) {
        rangeParam.setEndTime(LocalDateTime.now());
        rangeParam.setStartTime(DateUtil.toLocalDateTime(DateUtil.beginOfMonth((DateUtil.lastMonth().toCalendar()))));
        return Res.ok(rptTotalDayService.biScreenByCenter(pageParam,rangeParam,baseLevel));
    }

    @Operation(summary = "设备资产统计(参数为-1则表示中心级)")
    @GetMapping(value = "/assetsData")
    public ResResult<?> biScreenByAsset(
            Integer baseLevel
    ){

        Map<String,Object> result = new HashMap<>();
        result.put("equipmentAsset",deviceRecordsService.equipmentAssetStatistics(baseLevel));
        result.put("softWareAsset",softwareRecordsService.softWareAssetStatistics(baseLevel));
        return Res.ok(result);
    }

    @Operation(summary = "查询最近30天的MES工单统计")
    @GetMapping(value = "/statisticsWorkOrder")
    public ResResult<?> statisticsWorkOrder(
            RangeParam rangeParam,
            Integer baseLevel
    ){
        rangeParam.setStartTime(DateUtil.toLocalDateTime(DateUtil.beginOfMonth((DateUtil.lastMonth().toCalendar()))));
        rangeParam.setEndTime(LocalDateTimeUtil.endOfDay(LocalDateTime.now()));
        return Res.ok(rptTotalDayService.statisticsRptTotalDay(rangeParam,baseLevel));
    }


    @Operation(summary = "查询近30天的车间作业统计，支持基地区分")
    @GetMapping(value = "/shopFloorStatistics")
    public ResResult<?> shopFloorStatistics(
            RangeParam rangeParam,
            Integer baseLevel
    ){
        rangeParam.setStartTime(DateUtil.toLocalDateTime(DateUtil.beginOfMonth((DateUtil.lastMonth().toCalendar()))));
        rangeParam.setEndTime(LocalDateTimeUtil.endOfDay(LocalDateTime.now()));
        return Res.ok(rptTotalDayService.shopFloorStatistics(rangeParam,baseLevel));
    }


    @Operation(summary = "基地级生产看板")
    @GetMapping(value = "/baseProductionData")
    public ResResult<?> baseProductionData(
            RangeParam rangeParam
    ) {
        Map<String,Object> result = new HashMap<>();
        rangeParam.setEndTime(LocalDateTime.now());
        rangeParam.setStartTime(DateUtil.toLocalDateTime(DateUtil.beginOfMonth((DateUtil.lastMonth().toCalendar()))));
        return Res.ok(rptTotalDayService.baseProductionData(rangeParam));
    }

    @Operation(summary = "考勤记录数据看板")
    @GetMapping(value = "/attendanceRecord")
    public ResResult<?> attendanceRecord() {
        Map<String,Object> result = new HashMap<>();
        return Res.ok(acEntrytotalService.attemdanceDataPulicInterface());
    }


    @Operation(summary = "获取产线开机时间排名")
    @GetMapping("getProductionLineFirstBootRank")
    public ResResult<?> getProductionLineFirstBootRank(@Validated GetProductionLineFirstBootRankParam param, BindingResult bindingResult){
        String string = validBindResult(bindingResult);
        if (StringUtils.isNotBlank(string)) return Res.error(string);

        if (param.getStartDatetime() == null && param.getEndDatetime() == null && param.getEqDatetime() == null){
            Date date = new Date();
            param.setEqDatetime(DateUtil.beginOfDay(date));
        }
        return Res.ok(rcdProductionlineFirstBootService.getProductionLineFirstBootRank(param));
    }


    @Operation(summary = "获取产线开机条数")
    @GetMapping("getProductionLineBootCount")
    public ResResult<?> getProductionLineBootCount(@Validated GetProductionLineBootCountParam param, BindingResult bindingResult){
        String string = validBindResult(bindingResult);
        if (StringUtils.isNotBlank(string)) return Res.error(string);
        if (param.getStartDatetime() == null && param.getEndDatetime() == null && param.getEqDatetime() == null){
            Date date = new Date();
            param.setEqDatetime(DateUtil.beginOfDay(date));
        }

        return Res.ok(rcdProductionlineFirstBootService.getProductionLineBootCount(param));
    }

    @Operation(summary = "获取各产线最近n天开机情况")
    @GetMapping("getProductionLineBootReport")
    public ResResult<?> getProductionLineBootReport(@Validated GetProductionLineBootReportParam param, BindingResult bindingResult){
        String string = validBindResult(bindingResult);
        if (StringUtils.isNotBlank(string)) return Res.error(string);

        return Res.ok(rcdProductionlineFirstBootService.getProductionLineBootReport(param));
    }

    @Operation(summary = "获取各产线最近n天开机时长情况")
    @GetMapping("getProductionLineRunMinuteReport")
    public ResResult<?> getProductionLineRunMinuteReport(@Validated GetProductionLineRunMinuteReportParam param, BindingResult bindingResult){
        String string = validBindResult(bindingResult);
        if (StringUtils.isNotBlank(string)) return Res.error(string);

        return Res.ok(rcdRunminuteService.getProductionLineRunMinuteReport(param));
    }



    private String validBindResult(BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            StringBuilder sb =  new StringBuilder();
            bindingResult.getFieldErrors()
                    .forEach(e -> sb.append(e.getField())
                            .append(": ")
                            .append(e.getDefaultMessage())
                            .append("; "));
            return sb.toString();
        }
        return null;
    }


}
