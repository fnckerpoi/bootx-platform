package cn.bootx.platform.visualization.core.entity;

import java.time.LocalDateTime;

import cn.bootx.platform.common.core.function.EntityBaseFunction;
import cn.bootx.platform.common.mybatisplus.base.MpIdEntity;

import cn.bootx.platform.visualization.core.convert.SoftWareTracksConvert;
import cn.bootx.platform.visualization.dto.SoftWareTracksDto;
import cn.bootx.platform.visualization.param.SoftWareTracksParam;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* 
* @author Duriea
* @date 2023-05-19
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName("software_tracks")
public class SoftWareTracks extends MpIdEntity implements EntityBaseFunction<SoftWareTracksDto> {

    /**  */
    private Long softwareId;
    /**  */
    private Long deviceId;
    /**  */
    private LocalDateTime lendTime;
    /**  */
    private String lendDescription;
    /**  */
    private LocalDateTime planReturnTime;
    /**  */
    private LocalDateTime returnTime;
    /**  */
    private String returnDescription;
    /**  */
    private LocalDateTime deletedAt;
    /**  */
    private LocalDateTime createdAt;
    /**  */
    private LocalDateTime updatedAt;

    /** 创建对象 */
    public static SoftWareTracks init(SoftWareTracksParam in) {
            return SoftWareTracksConvert.CONVERT.convert(in);
    }

    /** 转换成dto */
    @Override
    public SoftWareTracksDto toDto() {
        return SoftWareTracksConvert.CONVERT.convert(this);
    }
}
