package cn.bootx.platform.visualization.core.dao;

import cn.bootx.platform.common.mybatisplus.impl.BaseManager;
import cn.bootx.platform.visualization.core.dao.MapperImpl.RcdProductionlineFirstBootMapper;
import cn.bootx.platform.visualization.core.entity.RcdProductionlineFirstBoot;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineBootCountParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineBootReportParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineFirstBootRankParam;
import cn.bootx.platform.visualization.vo.ProductionLineBootReportVO;
import cn.bootx.platform.visualization.vo.ProductionLineFirstBootRankVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public class RcdProductionlineFirstBootManager extends BaseManager<RcdProductionlineFirstBootMapper, RcdProductionlineFirstBoot> {
    public List<ProductionLineFirstBootRankVO> getProductionLineFirstBootRankList(GetProductionLineFirstBootRankParam param){
        return baseMapper.getProductionLineFirstBootRankList(param);
    }

    public List<ProductionLineBootReportVO> getProductionLineBootReportList(GetProductionLineBootReportParam param) {
        return baseMapper.getProductionLineBootReportList(param);
    }

    public ProductionLineBootReportVO getProductionLineInfoByFnumber(String code){
        return baseMapper.getProductionLineInfoByFnumber(code);
    }

    public Integer getProductionLineBootCount(GetProductionLineBootCountParam param) {
        return baseMapper.getProductionLineBootCount(param);
    }
}
