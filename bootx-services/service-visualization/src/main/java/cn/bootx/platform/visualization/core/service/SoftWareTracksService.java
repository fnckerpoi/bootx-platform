package cn.bootx.platform.visualization.core.service;

import cn.bootx.platform.common.core.util.ResultConvertUtil;
import cn.bootx.platform.visualization.code.GoVIewCode;
import cn.bootx.platform.visualization.core.dao.SoftWareTracksManager;
import cn.bootx.platform.visualization.core.entity.SoftWareTracks;
import cn.bootx.platform.visualization.dto.SoftWareTracksDto;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SoftWareTracksService {
    private final SoftWareTracksManager softWareTracksManager;

    /**
     * 获取全部
     */
    public List<SoftWareTracksDto> findAll(){
        return ResultConvertUtil.dtoListConvert(softWareTracksManager.findAll());
    }

    public List<SoftWareTracksDto> selectByMainIds(List<Long> softWareIds, Integer baseLevel) {
        List<SoftWareTracks> result = new ArrayList<>();
        if (ObjectUtil.isEmpty(baseLevel)){
            GoVIewCode.CHEMEX_LIST.forEach(dataSource -> {
                result.addAll(softWareTracksManager.selectByMainIds(dataSource, softWareIds));
            });
        }else{
            result.addAll(softWareTracksManager.selectByMainIds(GoVIewCode.CHEMEX_LIST.get(baseLevel-1),softWareIds ));
        }
        return ResultConvertUtil.dtoListConvert(result);
    }
}
