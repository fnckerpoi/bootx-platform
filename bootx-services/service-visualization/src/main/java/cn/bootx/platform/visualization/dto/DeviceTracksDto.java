package cn.bootx.platform.visualization.dto;

import java.time.LocalDateTime;

import cn.bootx.platform.common.core.rest.dto.BaseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(title = "")
@Accessors(chain = true)
public class DeviceTracksDto extends BaseDto {

    @Schema(description = "")
    private Long deviceId;
    @Schema(description = "")
    private Integer userId;
    @Schema(description = "")
    private LocalDateTime lendTime;
    @Schema(description = "")
    private String lendDescription;
    @Schema(description = "")
    private LocalDateTime planReturnTime;
    @Schema(description = "")
    private LocalDateTime returnTime;
    @Schema(description = "")
    private String returnDescription;
    @Schema(description = "")
    private LocalDateTime deletedAt;
    @Schema(description = "")
    private LocalDateTime createdAt;
    @Schema(description = "")
    private LocalDateTime updatedAt;

}
