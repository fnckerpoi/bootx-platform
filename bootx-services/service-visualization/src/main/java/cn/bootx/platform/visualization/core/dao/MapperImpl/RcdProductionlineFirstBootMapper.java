package cn.bootx.platform.visualization.core.dao.MapperImpl;

import cn.bootx.platform.visualization.param.bizParam.GetProductionLineBootCountParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineBootReportParam;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineFirstBootRankParam;
import cn.bootx.platform.visualization.vo.ProductionLineBootReportVO;
import cn.bootx.platform.visualization.vo.ProductionLineFirstBootRankVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.bootx.platform.visualization.core.entity.RcdProductionlineFirstBoot;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 记录每条产线每天最先开机时间(TRcdProductionlineFirstBoot)表数据库访问层
 *
 * @author makejava
 * @since 2024-01-30 16:08:30
 */
@Mapper
public interface RcdProductionlineFirstBootMapper extends BaseMapper<RcdProductionlineFirstBoot> {
    List<ProductionLineFirstBootRankVO> getProductionLineFirstBootRankList(
            @Param("param") @NotNull GetProductionLineFirstBootRankParam param
    );

    List<ProductionLineBootReportVO> getProductionLineBootReportList(@Param("param") @NotNull GetProductionLineBootReportParam param);

    ProductionLineBootReportVO getProductionLineInfoByFnumber(@Param("code")String code);

    Integer getProductionLineBootCount(@Param("param") GetProductionLineBootCountParam param);
}

