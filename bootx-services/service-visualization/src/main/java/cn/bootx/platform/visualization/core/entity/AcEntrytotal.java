package cn.bootx.platform.visualization.core.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import cn.bootx.platform.common.core.function.EntityBaseFunction;
import cn.bootx.platform.visualization.core.convert.AcEntrytotalConvert;
import cn.bootx.platform.visualization.dto.AcEntrytotalDto;
import cn.bootx.platform.visualization.param.AcEntrytotalParam;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* 考勤记录
* @author Duriea
* @since 2023-08-15
*/
@EqualsAndHashCode()
@Data
@Accessors(chain = true)
@TableName("t_ac_entrytotal")
public class AcEntrytotal implements EntityBaseFunction<AcEntrytotalDto> {

    /** 主键 */
    private Long fid;
    /** 创建时间 */
    private LocalDateTime fcreatedatetime;
    /** 创建人；取登录用户 */
    private String fcreateman;
    /** 最后修改时间 */
    private LocalDateTime flastmodifydatetime;
    /** 最后修改人；取用户名 */
    private String flastmodifyman;
    /**  */
    private LocalDateTime fbizdatetime;
    /**  */
    private String fnumber;
    /** 区域标识，与考勤系统wmmj库的tblLink表关联 */
    private String flinkid;
    /** 区域名称 */
    private String flinkname;
    /** 门禁标识，与考勤系统wmmj库的Ctrller表关联 */
    private Integer fctrlid;
    /** 门禁名称 */
    private String fctrlname;
    /** 所属车间，关联t_org_workshop */
    private Long fworkshopid;
    /** 所属车间全路径名 */
    private String fworkshopfullname;
    /** 开门次数 */
    private Integer fentrytotal;
    /**  */
    private BigDecimal fx;
    /**  */
    private BigDecimal fy;

    /** 创建对象 */
    public static AcEntrytotal init(AcEntrytotalParam in) {
            return AcEntrytotalConvert.CONVERT.convert(in);
    }

    /** 转换成dto */
    @Override
    public AcEntrytotalDto toDto() {
        return AcEntrytotalConvert.CONVERT.convert(this);
    }
}
