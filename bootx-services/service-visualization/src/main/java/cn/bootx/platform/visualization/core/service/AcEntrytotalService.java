package cn.bootx.platform.visualization.core.service;

import cn.bootx.platform.common.core.exception.DataNotExistException;
import cn.bootx.platform.common.core.rest.PageResult;
import cn.bootx.platform.common.core.rest.param.PageParam;
import cn.bootx.platform.common.core.util.ResultConvertUtil;
import cn.bootx.platform.common.mybatisplus.util.MpUtil;
import cn.bootx.platform.visualization.core.dao.AcEntrytotalManager;
import cn.bootx.platform.visualization.dto.AcEntrytotalDto;
import cn.bootx.platform.visualization.param.AcEntrytotalParam;
import cn.bootx.platform.visualization.core.entity.AcEntrytotal;
import cn.hutool.json.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 考勤记录
 * @author Duriea
 * @since 2023-08-15
 */
@Slf4j
@Service
@RequiredArgsConstructor
@DS("bigScreen")
public class AcEntrytotalService {
    private final AcEntrytotalManager acEntrytotalManager;

    /**
     * 添加
     */
    public void add(AcEntrytotalParam param){
        AcEntrytotal acEntrytotal = AcEntrytotal.init(param);
        acEntrytotalManager.save(acEntrytotal);
    }

    /**
     * 修改
     */
    public void update(AcEntrytotalParam param){
        AcEntrytotal acEntrytotal = acEntrytotalManager.findById(param.getId()).orElseThrow(DataNotExistException::new);

        BeanUtil.copyProperties(param,acEntrytotal, CopyOptions.create().ignoreNullValue());
        acEntrytotalManager.updateById(acEntrytotal);
    }

    /**
     * 分页
     */
    public PageResult<AcEntrytotalDto> page(PageParam pageParam,AcEntrytotalParam acEntrytotalParam){
        return MpUtil.convert2DtoPageResult(acEntrytotalManager.page(pageParam,acEntrytotalParam));
    }

    /**
     * 获取单条
     */
    public AcEntrytotalDto findById(Long id){
        return acEntrytotalManager.findById(id).map(AcEntrytotal::toDto).orElseThrow(DataNotExistException::new);
    }

    /**
     * 获取全部
     */
    public List<AcEntrytotalDto> findAll(){
        return ResultConvertUtil.dtoListConvert(acEntrytotalManager.findAll());
    }

    /**
     * 删除
     */
    public void delete(Long id){
        acEntrytotalManager.deleteById(id);
    }


    public Map<Object, Object> attemdanceDataPulicInterface() {
        List<AcEntrytotalDto> dataList = ResultConvertUtil.dtoListConvert(acEntrytotalManager.getLastRecord());
        Map<Object,Object> result = new HashMap<>();
        result.put("heatMap",attemdanceHeat(dataList));
        result.put("hotspotMap",hotspotMap(dataList));
        return result;
    }

    Map<Object,Object> attemdanceHeat(List<AcEntrytotalDto> srcDataList){
        Map<Object,Object> returnResult = new HashMap<>();
        List<String> xAxis = srcDataList.stream().map(AcEntrytotalDto::getFworkshopfullname).distinct().collect(Collectors.toList());
        List<String> yAxis = srcDataList.stream().map(AcEntrytotalDto::getFctrlname).distinct().collect(Collectors.toList());
        List<Object> seriesData = new ArrayList<>();
        srcDataList.forEach(a->{
            seriesData.add(Arrays.asList(
                    xAxis.indexOf(a.getFworkshopfullname()),
                    yAxis.indexOf(a.getFctrlname()),
                    a.getFentrytotal()
            ));
        });
        returnResult.put("xAxis",xAxis);
        returnResult.put("yAxis",yAxis);
        returnResult.put("seriesData",seriesData);

        return returnResult;
    }

    Map<Object,Object> hotspotMap(List<AcEntrytotalDto> srcDataList){
        Map<Object,Object> returnResult = new HashMap<>();
        List<Object> resultList = new ArrayList<>();
        srcDataList.forEach(a->{
            JSONObject rowData = new JSONObject();
            rowData.putOnce("name",a.getFlinkname()+"/"+a.getFctrlname());
            rowData.putOnce("value",a.getFentrytotal());
            rowData.putOnce("position", Arrays.asList(a.getFx(),a.getFy()));
            resultList.add(rowData);
        });
        returnResult.put("markers",resultList);
        return returnResult;
    }
}
