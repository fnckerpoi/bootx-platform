package cn.bootx.platform.visualization.param;

import java.time.LocalDateTime;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Data
@Schema(title = "")
@Accessors(chain = true)
public class SoftWareTracksParam {

    @Schema(description= "主键")
    private Long id;

    @Schema(description = "")
    private Long softwareId;
    @Schema(description = "")
    private Long deviceId;
    @Schema(description = "")
    private LocalDateTime lendTime;
    @Schema(description = "")
    private String lendDescription;
    @Schema(description = "")
    private LocalDateTime planReturnTime;
    @Schema(description = "")
    private LocalDateTime returnTime;
    @Schema(description = "")
    private String returnDescription;
    @Schema(description = "")
    private LocalDateTime deletedAt;
    @Schema(description = "")
    private LocalDateTime createdAt;
    @Schema(description = "")
    private LocalDateTime updatedAt;

}