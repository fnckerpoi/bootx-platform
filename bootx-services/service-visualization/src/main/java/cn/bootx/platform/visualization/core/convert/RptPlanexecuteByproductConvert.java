package cn.bootx.platform.visualization.core.convert;

import cn.bootx.platform.visualization.core.entity.RptPlanexecuteByproduct;
import cn.bootx.platform.visualization.dto.RptPlanexecuteByproductDto;
import cn.bootx.platform.visualization.param.RptPlanexecuteByproductParam;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 计划完成进度表，关注某产品每批累计生产进度，每个品种产品，以月自然日1-30号生产记录，有排产数量或完工数量的，填入对应的品种日期，无的，填0
 * @author luokanran
 * @since 2023-09-18
 */
@Mapper
public interface RptPlanexecuteByproductConvert {
    RptPlanexecuteByproductConvert CONVERT = Mappers.getMapper(RptPlanexecuteByproductConvert.class);

    RptPlanexecuteByproduct convert(RptPlanexecuteByproductParam in);

    RptPlanexecuteByproductDto convert(RptPlanexecuteByproduct in);

}
