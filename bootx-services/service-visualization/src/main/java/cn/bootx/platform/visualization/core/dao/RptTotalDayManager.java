package cn.bootx.platform.visualization.core.dao;

import cn.bootx.platform.common.core.rest.param.RangeParam;
import cn.bootx.platform.common.core.util.ResultConvertUtil;
import cn.bootx.platform.common.mybatisplus.impl.BaseManager;

import cn.bootx.platform.visualization.core.dao.MapperImpl.RptTotalDayMapper;
import cn.bootx.platform.visualization.core.entity.RptTotalDay;
import cn.bootx.platform.visualization.dto.RptTotalDayDto;
import cn.bootx.platform.visualization.param.RptTotalDayParam;
import cn.bootx.platform.visualization.param.enumParam.PerfectBIScreeEnum;
import cn.hutool.core.util.EnumUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import cn.bootx.platform.visualization.param.enumParam.PerfectBIScreeEnum.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * MES类-按日统计的数据
 *
 * @author Duriea
 * @date 2023-05-15
 */
@Repository
@RequiredArgsConstructor
public class RptTotalDayManager extends BaseManager<RptTotalDayMapper, RptTotalDay> {

    public List<RptTotalDay> selectByCenter(RptTotalDayParam pageParam, RangeParam rangeParam, Integer baseLevel) {

        LambdaQueryChainWrapper<RptTotalDay> result = lambdaQuery();

        if (ObjectUtil.isEmpty(baseLevel)){
            result.eq(RptTotalDay::getFareatype, PerfectBIScreenRangeLevelEnum.CENTER.getKey());
        }else{
            result.eq(RptTotalDay::getFareaid,baseLevel)
                    .in(RptTotalDay::getFtypeid, Arrays.stream(PerfectBIScreenDataTypeEnumOfCenter.values())
                            .map(PerfectBIScreenDataTypeEnumOfCenter::getKey)
                            .collect(Collectors.toList()));
        }

        if (ObjectUtil.isNotEmpty(rangeParam.getStartTime())) {
            result.ge(RptTotalDay::getFdate, rangeParam.getStartTime());
        }
        if (ObjectUtil.isNotEmpty(rangeParam.getEndTime())){
            result.le(RptTotalDay::getFdate, rangeParam.getEndTime());
        }

        return result.list();
    }

    public List<RptTotalDayDto> statisticsRptTotalDay(RangeParam rangeParam, Integer baseLevel) {
        LambdaQueryChainWrapper<RptTotalDay> result = lambdaQuery()
                .in(RptTotalDay::getFtypeid,
                        EnumUtil.getFieldValues(PerfectBIScreeEnum.MesWorkOrderTypeEnum.class,"key")
                );

        if (ObjectUtil.isNotEmpty(baseLevel)){
            result.eq(RptTotalDay::getFareaid,baseLevel);
        }

        if (ObjectUtil.isNotEmpty(rangeParam.getStartTime())) {
            result.ge(RptTotalDay::getFdate, rangeParam.getStartTime());
        }
        if (ObjectUtil.isNotEmpty(rangeParam.getEndTime())){
            result.le(RptTotalDay::getFdate, rangeParam.getEndTime());
        }

        return ResultConvertUtil.dtoListConvert(result.list());

    }

    public List<RptTotalDay> selectBaseData(RangeParam rangeParam, Integer baseLevel) {
        LambdaQueryChainWrapper<RptTotalDay> result = lambdaQuery()
                .eq(RptTotalDay::getFareatype,PerfectBIScreenRangeLevelEnum.BASE.getKey())
                .in(RptTotalDay::getFtypeid, EnumUtil.getFieldValues(PerfectBIScreeEnum.PerfectBIScreenDataTypeEnumOfBase.class, "key"));

        if (ObjectUtil.isNotEmpty(baseLevel)){
            result.eq(RptTotalDay::getFareaid,baseLevel);
        }
        if (ObjectUtil.isNotEmpty(rangeParam.getStartTime())) {
            result.ge(RptTotalDay::getFdate, rangeParam.getStartTime());
        }
        if (ObjectUtil.isNotEmpty(rangeParam.getEndTime())){
            result.le(RptTotalDay::getFdate,rangeParam.getEndTime());
        }
        return result.list();
    }


}
