package cn.bootx.platform.visualization.core.service;

import cn.bootx.platform.visualization.core.dao.RcdProductionlineFirstBootManager;
import cn.bootx.platform.visualization.core.dao.RcdRunminuteManager;
import cn.bootx.platform.visualization.echarts.EchartsDataSet;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineRunMinuteReportParam;
import cn.bootx.platform.visualization.vo.ProductionLineBootReportVO;
import cn.bootx.platform.visualization.vo.ProductionLineRunMinuteVO;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@DS("m_cube@zs")
@Slf4j
@Service
@AllArgsConstructor
public class TRcdRunminuteService {

    private final RcdRunminuteManager rcdRunminuteManager;
    private final RcdProductionlineFirstBootManager rcdProductionlineFirstBootManager;

    public Map<String,Object> getProductionLineRunMinuteReport(GetProductionLineRunMinuteReportParam param) {
        List<ProductionLineRunMinuteVO> voList = rcdRunminuteManager.getProductionLineRunMinuteReport(param);
        EchartsDataSet<Map<String,Object>> dataset = EchartsDataSet.create();
        // 构建dimensions
        dataset.addDimension("day");
        TreeMap<String,Map<String,Object>> cache = new TreeMap<>();

        if (CollectionUtils.isEmpty(voList) && param.getType() == 1) {
            // 根据产线编号查出产线信息
            ProductionLineBootReportVO productionLineBootReportVO = rcdProductionlineFirstBootManager.getProductionLineInfoByFnumber(param.getSearchCode());
            dataset.addDimension(productionLineBootReportVO.getFactoryName() + "/" + productionLineBootReportVO.getWorkshopName() + "/" + productionLineBootReportVO.getProductionlineName());
        }

        voList.forEach(item -> {
            String productionLineName = item.getFactoryName() + "/" + item.getWorkshopName() + "/" + item.getProductionlineName();
            dataset.addDimension(productionLineName);
            String[] split = item.getRunMinuteStr().split(",");
            for (String string : split) {
                if (StringUtils.isBlank(string)) continue;

                String[] split1 = string.split("\\|");
                String dateStr = split1[0];
                String runMinuteStr = split1[1];
                if(cache.containsKey(dateStr)){
                    cache.get(dateStr).put(productionLineName,runMinuteStr);
                    continue;
                }
                cache.put(dateStr,new HashMap<String,Object>(){{
                    put(productionLineName,runMinuteStr);
                }});
            }
        });

        cache.forEach((k,v) -> dataset.addSource(new HashMap<String,Object>(){{
            put("day",k);
            putAll(v);
        }}));


        return dataset.toMap();
    }
}
