package cn.bootx.platform.visualization.core.dao.MapperImpl;

import cn.bootx.platform.visualization.core.entity.RptTotalDay;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * MES类-按日统计的数据
 * @author Duriea
 * @date 2023-05-15
 */
@Mapper
public interface RptTotalDayMapper extends BaseMapper<RptTotalDay> {
}