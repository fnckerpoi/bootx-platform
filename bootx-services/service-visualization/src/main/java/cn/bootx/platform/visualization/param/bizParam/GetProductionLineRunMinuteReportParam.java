package cn.bootx.platform.visualization.param.bizParam;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
@Schema(name = "获取各产线最近n天开机时长对比参数(返回dataset)")
public class GetProductionLineRunMinuteReportParam {

    @Schema(description = "查询类型: 0 = 查询所有产线, 1 = 按产线编号查, 2 = 按车间编号查, 3 = 按工厂编号查, 4 = 按公司编号查询")
    @NotNull
    private Integer type;

    @Schema(description = "查询多少天")
    @NotNull
    @Min(0)
    private Integer day;

    @Schema(description = "需要查询的编号")
    private String searchCode;

}
