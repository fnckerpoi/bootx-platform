package cn.bootx.platform.visualization.param.bizParam;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Accessors(chain = true)
@Schema(name = "获取产线开机条数接口参数对象")
public class GetProductionLineBootCountParam {

    @Schema(description = "查询类型: 0 = 查询所有产线, 1 = 按产线编号查, 2 = 按车间编号查, 3 = 按工厂编号查, 4 = 按公司编号查询")
    @NotNull
    private Integer type;


    @Schema(description = "需要查询的编号")
    private String searchCode;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "时间精确匹配，传了这个参数则不走范围查询")
    private Date eqDatetime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "开始时间")
    private Date startDatetime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "结束时间")
    private Date endDatetime;
}
