package cn.bootx.platform.visualization.core.dao;

import cn.bootx.platform.common.mybatisplus.impl.BaseManager;

import cn.bootx.platform.visualization.core.dao.MapperImpl.SoftwareRecordsMapper;
import cn.bootx.platform.visualization.core.entity.SoftwareRecords;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Repository
@RequiredArgsConstructor
public class SoftwareRecordsManager extends BaseManager<SoftwareRecordsMapper, SoftwareRecords> {

    @DS("#dataSource")
    public List<SoftwareRecords> selectSoftWare(String dataSource) {
        return lambdaQuery().list();
    }
}
