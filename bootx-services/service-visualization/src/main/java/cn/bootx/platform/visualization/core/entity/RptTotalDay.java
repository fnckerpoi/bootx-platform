package cn.bootx.platform.visualization.core.entity;

import cn.bootx.platform.common.core.function.EntityBaseFunction;
import cn.bootx.platform.visualization.core.convert.RptTotalDayConvert;
import cn.bootx.platform.visualization.dto.RptTotalDayDto;
import cn.bootx.platform.visualization.param.RptTotalDayParam;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
* MES类-按日统计的数据
* @author Duriea
* @date 2023-05-15
*/
@EqualsAndHashCode(callSuper = false)
@Data
@Accessors(chain = true)
@TableName("t_rpt_total_day")
public class RptTotalDay implements EntityBaseFunction<RptTotalDayDto> {

    /** 主键 */
    private Long fid;
    /** 创建时间 */
    private LocalDateTime fcreatedatetime;
    /** 创建人；取登录用户 */
    private String fcreateman;
    /** 最后修改时间 */
    private LocalDateTime flastmodifydatetime;
    /** 最后修改人；取用户名 */
    private String flastmodifyman;
    /** 1:广东-生产结束工单总数；2：广东-审核完毕工单总数  3：广东-未开始审核工单数量  4：广东-审核部分完成数量  5:广东-无需审核数量 */
    private Integer ftypeid;
    /** 日期 */
    private LocalDateTime fdate;
    /** 数量或比例值 */
    private BigDecimal fqty;
    /** 指标数据有特殊情况时，在此栏说明 */
    private String fmemo;
    /** 区域类别：  0：中心  1：基地   2：厂级  3：车间  4：产线 */
    private Integer fareatype;
    /** 区域id。区域类别：中心：此值无效基地：关联t_org_company:fid 厂级：关联t_org_factory:fid 车间级：关联t_org_workshop:fid 产线级：关联t_org_productline:fid */
    private String fareaid;

    /** 创建对象 */
    public static RptTotalDay init(RptTotalDayParam in) {
            return RptTotalDayConvert.CONVERT.convert(in);
    }

    /** 转换成dto */
    @Override
    public RptTotalDayDto toDto() {
        return RptTotalDayConvert.CONVERT.convert(this);
    }
}
