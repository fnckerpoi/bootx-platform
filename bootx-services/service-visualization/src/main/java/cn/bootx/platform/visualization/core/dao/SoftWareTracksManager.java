package cn.bootx.platform.visualization.core.dao;

import cn.bootx.platform.common.mybatisplus.impl.BaseManager;
import cn.bootx.platform.visualization.core.dao.MapperImpl.SoftWareTracksMapper;

import cn.bootx.platform.visualization.core.entity.SoftWareTracks;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Repository
@RequiredArgsConstructor
public class SoftWareTracksManager extends BaseManager<SoftWareTracksMapper, SoftWareTracks> {
    @DS("#dataSource")
    public List<SoftWareTracks> selectByMainIds(String dataSource, List<Long> softWareIds) {
        return lambdaQuery().in(SoftWareTracks::getSoftwareId,softWareIds).list();
    }
}
