package cn.bootx.platform.visualization.core.dao.MapperImpl;

import cn.bootx.platform.visualization.core.entity.TRcdRunminute;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineRunMinuteReportParam;
import cn.bootx.platform.visualization.vo.ProductionLineRunMinuteVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TRcdRunminuteMapper extends BaseMapper<TRcdRunminute> {
    List<ProductionLineRunMinuteVO> getProductionLineRunMinuteReport(@Param("param") GetProductionLineRunMinuteReportParam param);

}
