package cn.bootx.platform.visualization.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class ProductionLineFirstBootRankVO {
    // 主键
    private Long fid;
    // 首次开机时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date ffirstbootdatetime;
    // 业务日期
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date fdate;
    // 产线编号
    private String productionlineNumber;
    // 产线线Id
    private Long productionlineId;

    // 产线名称
    private String productionlineName;

    // 车间名称
    private String workshopName;
    // 车间编号
    private String workshopNumber;
    // 工厂名称
    private String factoryName;
    // 工厂编号
    private String factoryNumber;
    // 公司名称
    private String companyName;
    // 公司编号
    private String companyNumber;

    // 产线全称
    private String productionlineFullName;
}
