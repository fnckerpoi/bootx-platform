package cn.bootx.platform.visualization.core.service;

import cn.bootx.platform.common.core.rest.dto.KeyValue;
import cn.bootx.platform.common.core.util.LocalDateTimeUtil;
import cn.bootx.platform.common.core.util.ResultConvertUtil;
import cn.bootx.platform.visualization.code.GoVIewCode;
import cn.bootx.platform.visualization.core.dao.DeviceRecordsManager;
import cn.bootx.platform.visualization.core.entity.DeviceRecords;
import cn.bootx.platform.visualization.dto.DeviceRecordsDto;
import cn.bootx.platform.visualization.dto.DeviceTracksDto;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 
 * @author Duriea
 * @date 2023-05-18
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DeviceRecordsService {
    private final DeviceRecordsManager deviceRecordsManager;
    private final DeviceTracksService deviceTracksService;

    public List<DeviceRecordsDto> selectComputer(Integer baseLevel){
        List<DeviceRecords> result = new ArrayList<>();
        if (ObjectUtil.isEmpty(baseLevel)) {
            GoVIewCode.CHEMEX_LIST.forEach(dataSource->{
                result.addAll(deviceRecordsManager.selectComputer(dataSource));
            });
        }else{
            result.addAll(deviceRecordsManager.selectComputer(GoVIewCode.CHEMEX_LIST.get(baseLevel-1)));
        }

        return ResultConvertUtil.dtoListConvert(result);
    }


    public Map<String,Object> equipmentAssetStatistics(Integer baseLevel){
        Map<String, Object> result = new HashMap<>();

        List<DeviceRecordsDto> records = this.selectComputer(baseLevel);

        List<DeviceTracksDto> tracks = deviceTracksService.selectByMainIds(
                records.stream().map(DeviceRecordsDto::getId).collect(Collectors.toList()),
                baseLevel
        );

        List<DeviceRecordsDto> registeredNum =  records.stream().filter(a->a.getDeletedAt()  == null).collect(Collectors.toList());
        long newItAssetsAddedToday = records.stream().filter(a-> LocalDateTimeUtil.isSameDay(a.getCreatedAt(),LocalDateTimeUtil.now())).count();
        long turnIdle = tracks.stream().filter(a->LocalDateTimeUtil.isSameDay(a.getDeletedAt(), LocalDateTime.now())).count();
        long usingDevice = tracks.stream().filter(a->a.getDeletedAt() == null).count();
        long idleIngDevice = registeredNum.size() - usingDevice;


        List<KeyValue> source = new ArrayList<>();
        source.add(new KeyValue("采购在册数量",String.valueOf(registeredNum.size())));
        source.add(new KeyValue("今日日新增资产",String.valueOf(newItAssetsAddedToday)));
        source.add(new KeyValue("今日转闲置数量",String.valueOf(turnIdle)));
        source.add(new KeyValue("使用数量",String.valueOf(usingDevice)));
        source.add(new KeyValue("闲置数量",String.valueOf(idleIngDevice)));

        result.put("source",source);
        result.put("dimensions", Arrays.asList("key","value"));
        return result;
    }
}
