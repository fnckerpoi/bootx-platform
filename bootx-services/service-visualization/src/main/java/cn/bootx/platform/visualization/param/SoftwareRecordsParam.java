package cn.bootx.platform.visualization.param;

import java.time.LocalDateTime;
import java.time.LocalDate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Data
@Schema(title = "")
@Accessors(chain = true)
public class SoftwareRecordsParam {

    @Schema(description= "主键")
    private Long id;

    @Schema(description = "")
    private String name;
    @Schema(description = "")
    private String description;
    @Schema(description = "")
    private Integer categoryId;
    @Schema(description = "")
    private Integer vendorId;
    @Schema(description = "")
    private String sn;
    @Schema(description = "")
    private Double price;
    @Schema(description = "")
    private LocalDate purchased;
    @Schema(description = "")
    private LocalDate expired;
    @Schema(description = "")
    private String distribution;
    @Schema(description = "")
    private Integer counts;
    @Schema(description = "")
    private String assetNumber;
    @Schema(description = "")
    private LocalDateTime deletedAt;
    @Schema(description = "")
    private LocalDateTime createdAt;
    @Schema(description = "")
    private LocalDateTime updatedAt;

}