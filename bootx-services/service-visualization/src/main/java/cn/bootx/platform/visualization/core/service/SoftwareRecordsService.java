package cn.bootx.platform.visualization.core.service;

import cn.bootx.platform.common.core.rest.dto.KeyValue;
import cn.bootx.platform.common.core.util.ResultConvertUtil;
import cn.bootx.platform.visualization.code.GoVIewCode;
import cn.bootx.platform.visualization.core.dao.SoftwareRecordsManager;
import cn.bootx.platform.visualization.core.entity.SoftwareRecords;
import cn.bootx.platform.visualization.dto.SoftWareTracksDto;
import cn.bootx.platform.visualization.dto.SoftwareRecordsDto;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SoftwareRecordsService {
    private final SoftwareRecordsManager softwareRecordsManager;
    private final SoftWareTracksService softWareTracksService;

    /**
     * 获取全部
     */
    public List<SoftwareRecordsDto> findAll(){
        return ResultConvertUtil.dtoListConvert(softwareRecordsManager.findAll());
    }



    public Map<String,Object> softWareAssetStatistics(Integer baseLevel) {
        Map<String,Object> result = new HashMap<>();

        List<SoftwareRecordsDto> records = this.selectSoftWare(baseLevel);
        List<Long> softWareIds = records.stream().map(SoftwareRecordsDto::getId).collect(Collectors.toList());
        List<SoftWareTracksDto> tracks = softWareTracksService.selectByMainIds(softWareIds,baseLevel);

        //软件使用数量
        long softWareUsed = tracks.stream().filter(a->a.getDeletedAt()==null).count();
        long assetCount = records.stream()
                .map(SoftwareRecordsDto::getAssetcount)
                .map(Optional::ofNullable)
                .mapToInt(opt-> opt.orElse(0))
                .sum();
        //闲置数量
        long softwareIdles = assetCount - softWareUsed;

        //软件持有数量
        long softwareNum = records.size();
        //软件使用记录数
        long usingSoftwareNum = tracks.stream().map(SoftWareTracksDto::getSoftwareId).distinct().count();
        //软件闲置记录数
        long idleSoftwareNum  = softwareNum - usingSoftwareNum;

        List<KeyValue> source = new ArrayList<>();
        source.add(new KeyValue("软件持有数量",String.valueOf(softwareNum)));
        source.add(new KeyValue("软件使用记录数",String.valueOf(usingSoftwareNum)));
        source.add(new KeyValue("软件闲置记录数",String.valueOf(idleSoftwareNum)));
        source.add(new KeyValue("使用数量",String.valueOf(softWareUsed)));
        source.add(new KeyValue("闲置数量",String.valueOf(softwareIdles)));

        result.put("dimensions",Arrays.asList("key","value"));
        result.put("source",source);
        return result;
    }

    private List<SoftwareRecordsDto> selectSoftWare(Integer baseLevel) {
        List<SoftwareRecords> result = new ArrayList<>();
        if (ObjectUtil.isEmpty(baseLevel)) {
            GoVIewCode.CHEMEX_LIST.forEach(a->{
                result.addAll(softwareRecordsManager.selectSoftWare(a));
            });
        }else{
            result.addAll(softwareRecordsManager.selectSoftWare(GoVIewCode.CHEMEX_LIST.get(baseLevel-1)));
        }

        return ResultConvertUtil.dtoListConvert(result);
    }
}
