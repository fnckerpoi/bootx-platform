package cn.bootx.platform.visualization.core.dao.MapperImpl;

import cn.bootx.platform.visualization.core.entity.SoftwareRecords;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Mapper
public interface SoftwareRecordsMapper extends BaseMapper<SoftwareRecords> {
}