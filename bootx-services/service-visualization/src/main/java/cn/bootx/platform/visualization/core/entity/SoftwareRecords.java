package cn.bootx.platform.visualization.core.entity;

import java.time.LocalDateTime;
import java.time.LocalDate;

import cn.bootx.platform.common.core.function.EntityBaseFunction;
import cn.bootx.platform.common.mybatisplus.base.MpIdEntity;

import cn.bootx.platform.visualization.core.convert.SoftwareRecordsConvert;
import cn.bootx.platform.visualization.dto.SoftwareRecordsDto;
import cn.bootx.platform.visualization.param.SoftwareRecordsParam;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* 
* @author Duriea
* @date 2023-05-19
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName("software_records")
public class SoftwareRecords extends MpIdEntity implements EntityBaseFunction<SoftwareRecordsDto> {

    /**  */
    private String name;
    /**  */
    private String description;
    /**  */
    private Integer categoryId;
    /**  */
    private Integer vendorId;
    /**  */
    private String sn;
    /**  */
    private Double price;
    /**  */
    private LocalDate purchased;
    /**  */
    private LocalDate expired;
    /**  */
    private String distribution;
    /**  */
    private Integer counts;
    /**  */
    private String assetNumber;
    /**  */
    private LocalDateTime deletedAt;
    /**  */
    private LocalDateTime createdAt;
    /**  */
    private LocalDateTime updatedAt;
    /**  */
    private Integer assetcount;

    /** 创建对象 */
    public static SoftwareRecords init(SoftwareRecordsParam in) {
            return SoftwareRecordsConvert.CONVERT.convert(in);
    }

    /** 转换成dto */
    @Override
    public SoftwareRecordsDto toDto() {
        return SoftwareRecordsConvert.CONVERT.convert(this);
    }
}
