package cn.bootx.platform.visualization.param.bizParam;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
@Schema(name = "获取最近n日开机时长参数")
public class GetProductionLineBootReportParam {
    @Schema(description = "查询类型: 0 = 查询所有产线, 1 = 按产线编号查, 2 = 按车间编号查, 3 = 按工厂编号查, 4 = 按公司编号查询")
    @NotNull
    private Integer type;

    @Schema(description = "查询多少天")
    @NotNull
    @Min(0)
    private Integer day;

    @Schema(description = "需要查询的编号")
    private String searchCode;

    @Schema(description = "是否需要转化为特殊格式")
    private Boolean toSpecial = false;

    @Schema(description = "seriesType")
    private String seriesType = "line";

    @Schema(description = "name模板,自动将key替换成对于的内容，支持的key:['company','factory','workshop','productionline']")
    private String template = "productionline";

}
