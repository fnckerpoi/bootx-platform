package cn.bootx.platform.visualization.core.dao.MapperImpl;

import cn.bootx.platform.visualization.core.entity.DeviceRecords;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author xxm
 * @date 2023-05-18
 */
@Mapper
public interface DeviceRecordsMapper extends BaseMapper<DeviceRecords> {
}