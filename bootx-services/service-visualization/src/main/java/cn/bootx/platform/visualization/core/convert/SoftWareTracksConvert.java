package cn.bootx.platform.visualization.core.convert;

import cn.bootx.platform.visualization.core.entity.SoftWareTracks;
import cn.bootx.platform.visualization.dto.SoftWareTracksDto;
import cn.bootx.platform.visualization.param.SoftWareTracksParam;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Mapper
public interface SoftWareTracksConvert {
    SoftWareTracksConvert CONVERT = Mappers.getMapper(SoftWareTracksConvert.class);

    SoftWareTracks convert(SoftWareTracksParam in);

    SoftWareTracksDto convert(SoftWareTracks in);

}