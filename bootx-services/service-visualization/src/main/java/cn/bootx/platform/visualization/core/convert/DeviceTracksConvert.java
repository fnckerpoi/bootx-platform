package cn.bootx.platform.visualization.core.convert;

import cn.bootx.platform.visualization.core.entity.DeviceTracks;
import cn.bootx.platform.visualization.dto.DeviceTracksDto;
import cn.bootx.platform.visualization.param.DeviceTracksParam;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Mapper
public interface DeviceTracksConvert {
    DeviceTracksConvert CONVERT = Mappers.getMapper(DeviceTracksConvert.class);

    DeviceTracks convert(DeviceTracksParam in);

    DeviceTracksDto convert(DeviceTracks in);

}