package cn.bootx.platform.visualization.core.convert;

import cn.bootx.platform.visualization.core.entity.DeviceRecords;
import cn.bootx.platform.visualization.dto.DeviceRecordsDto;
import cn.bootx.platform.visualization.param.DeviceRecordsParam;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 
 * @author xxm
 * @date 2023-05-18
 */
@Mapper
public interface DeviceRecordsConvert {
    DeviceRecordsConvert CONVERT = Mappers.getMapper(DeviceRecordsConvert.class);

    DeviceRecords convert(DeviceRecordsParam in);

    DeviceRecordsDto convert(DeviceRecords in);

}