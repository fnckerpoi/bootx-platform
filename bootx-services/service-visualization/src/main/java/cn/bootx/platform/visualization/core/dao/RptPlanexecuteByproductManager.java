package cn.bootx.platform.visualization.core.dao;

import cn.bootx.platform.common.mybatisplus.impl.BaseManager;
import cn.bootx.platform.visualization.core.dao.MapperImpl.RptPlanexecuteByproductMapper;
import cn.bootx.platform.visualization.param.RptPlanexecuteByproductParam;
import cn.bootx.platform.visualization.core.entity.RptPlanexecuteByproduct;
import cn.bootx.platform.common.core.rest.param.PageParam;
import cn.bootx.platform.common.mybatisplus.util.MpUtil;
import cn.bootx.platform.common.query.generator.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

/**
 * 计划完成进度表，关注某产品每批累计生产进度，每个品种产品，以月自然日1-30号生产记录，有排产数量或完工数量的，填入对应的品种日期，无的，填0
 * @author luokanran
 * @since 2023-09-18
 */
@Repository
@RequiredArgsConstructor
public class RptPlanexecuteByproductManager extends BaseManager<RptPlanexecuteByproductMapper, RptPlanexecuteByproduct> {

    /**
    * 分页
    */
    public Page<RptPlanexecuteByproduct> page(PageParam pageParam, RptPlanexecuteByproductParam param) {
        Page<RptPlanexecuteByproduct> mpPage = MpUtil.getMpPage(pageParam, RptPlanexecuteByproduct.class);
        QueryWrapper<RptPlanexecuteByproduct> wrapper = QueryGenerator.generator(param, this.getEntityClass());
        wrapper.select(this.getEntityClass(),MpUtil::excludeBigField)
                .orderByDesc(MpUtil.getColumnName(RptPlanexecuteByproduct::getFid));
        return this.page(mpPage,wrapper);
    }
}
