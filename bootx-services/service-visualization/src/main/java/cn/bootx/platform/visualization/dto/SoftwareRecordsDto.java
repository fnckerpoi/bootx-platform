package cn.bootx.platform.visualization.dto;

import java.time.LocalDateTime;
import java.time.LocalDate;

import cn.bootx.platform.common.core.rest.dto.BaseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(title = "")
@Accessors(chain = true)
public class SoftwareRecordsDto extends BaseDto {

    @Schema(description = "")
    private String name;
    @Schema(description = "")
    private String description;
    @Schema(description = "")
    private Integer categoryId;
    @Schema(description = "")
    private Integer vendorId;
    @Schema(description = "")
    private String sn;
    @Schema(description = "")
    private Double price;
    @Schema(description = "")
    private LocalDate purchased;
    @Schema(description = "")
    private LocalDate expired;
    @Schema(description = "")
    private String distribution;
    @Schema(description = "")
    private Integer counts;
    @Schema(description = "")
    private String assetNumber;
    @Schema(description = "")
    private LocalDateTime deletedAt;
    @Schema(description = "")
    private LocalDateTime createdAt;
    @Schema(description = "")
    private LocalDateTime updatedAt;
    @Schema(description = "")
    private Integer assetcount;

}
