package cn.bootx.platform.visualization.core.convert;

import cn.bootx.platform.visualization.core.entity.SoftwareRecords;
import cn.bootx.platform.visualization.dto.SoftwareRecordsDto;
import cn.bootx.platform.visualization.param.SoftwareRecordsParam;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 
 * @author Duriea
 * @date 2023-05-19
 */
@Mapper
public interface SoftwareRecordsConvert {
    SoftwareRecordsConvert CONVERT = Mappers.getMapper(SoftwareRecordsConvert.class);

    SoftwareRecords convert(SoftwareRecordsParam in);

    SoftwareRecordsDto convert(SoftwareRecords in);

}