package cn.bootx.platform.visualization.core.dao.MapperImpl;

import cn.bootx.platform.visualization.core.entity.AcEntrytotal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 考勤记录
 * @author Duriea
 * @since 2023-08-15
 */
@Mapper
public interface AcEntrytotalMapper extends BaseMapper<AcEntrytotal> {
    public List<AcEntrytotal> getLastRecord();
}
