package cn.bootx.platform.visualization.param.enumParam;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.*;

/**
 * @author duriea
 */
public class PerfectBIScreeEnum {

    @ToString
    @Getter
    @AllArgsConstructor
    public enum PerfectBIScreenRangeLevelEnum {
        CENTER(0, "中心"),
        BASE(1, "基地"),
        FACTORY1(2, "工厂级"),
        WORKSHOP(3, "车间"),
        PRODUCTIONLINE(4, "产线");
        private final Integer key;
        private final String value;
    }

    @ToString
    @Getter
    @AllArgsConstructor
    public enum PerfectBIScreenDataTypeEnumOfCenter {
        newSchedulesAddedPerDay(11, "每日新增排程数量"),
        numOfPreSpray(12, "预喷任务数量"),
        notBatchProductionRecord(13, "无批号生产记录"),
        monthlyOEemByOdm(14, "月度OEM对比ODM"),
        pleaseCheckTheNumberOfSemiFinishedProducts(15, "半成品请检数"),
        centerOutput(23,"制造中心总产量"),
        oemOdmOrderQty(24,"OEM/ODM订货数量")
        ;

        public static PerfectBIScreenDataTypeEnumOfCenter getEnum(Integer key) {
            for (PerfectBIScreenDataTypeEnumOfCenter e : PerfectBIScreenDataTypeEnumOfCenter.values()) {
                if (e.key.equals(key)) {
                    return e;
                }
            }
            return null;
        }

        private final Integer key;
        private final String value;
    }


    @ToString
    @Getter
    @AllArgsConstructor
    public enum PerfectBIScreenDataTypeEnumOfBase {
        newSchedulesAddedPerDay(11, "每日新增排程数量"),
        samplingTest(17, "抽样次数"),
        bigClearance(18, "大清场次数"),
        smallClearance(19, "小清场次数"),
        materialWeighing(20, "物料称量次数"),
        electronicScaleNum(21, "MES电子称总数"),
        electronicScaleValidExpiration(22, "MES电子称有效到期数量"),
        baseOutput(23,"基地产量");


        public static PerfectBIScreenDataTypeEnumOfBase getEnum(Integer key) {
            for (PerfectBIScreenDataTypeEnumOfBase e : PerfectBIScreenDataTypeEnumOfBase.values()) {
                if (e.key.equals(key)) {
                    return e;
                }
            }
            return null;
        }

        private final Integer key;
        private final String value;
    }

    @ToString
    @Getter
    @AllArgsConstructor
    public enum MesWorkOrderTypeEnum {
        endOfProduction(1, "生产结束单数"),
        reviewed(2, "审核完毕单数"),
        reviewNotStarted(3, "未开始审核单数"),
        auditPartComplete(4, "审核部分单数"),
        noReviewRequired(5, "无需审核单数");


        public static String getValue(Integer key) {
            for (MesWorkOrderTypeEnum e : MesWorkOrderTypeEnum.values()) {
                if (e.key.equals(key)) {
                    return e.value;
                }
            }
            return null;
        }

        private final Integer key;
        private final String value;
    }

    @ToString
    @Getter
    @AllArgsConstructor
    public enum PerfectBaseName {
        ZHONGSHAN("1", "中山"),
        YANGZHOU("2", "扬州"),
        HUAIBEI("164854454930165", "淮北");
        public static String getValue(String key) {
            for (PerfectBaseName e : PerfectBaseName.values()) {
                if (e.key.equals(key)) {
                    return e.value;
                }
            }
            return null;
        }
        private final String key;
        private final String value;
    }
}


