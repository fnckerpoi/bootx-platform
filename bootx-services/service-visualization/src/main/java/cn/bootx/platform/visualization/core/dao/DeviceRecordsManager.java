package cn.bootx.platform.visualization.core.dao;

import cn.bootx.platform.common.core.rest.param.PageParam;
import cn.bootx.platform.common.mybatisplus.base.MpIdEntity;
import cn.bootx.platform.common.mybatisplus.impl.BaseManager;
import cn.bootx.platform.common.mybatisplus.util.MpUtil;

import cn.bootx.platform.visualization.code.GoVIewCode;
import cn.bootx.platform.visualization.core.dao.MapperImpl.DeviceRecordsMapper;
import cn.bootx.platform.visualization.core.entity.DeviceRecords;
import cn.bootx.platform.visualization.param.DeviceRecordsParam;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;


/**
 * 
 * @author xxm
 * @date 2023-05-18
 */
@Repository
@RequiredArgsConstructor
public class DeviceRecordsManager extends BaseManager<DeviceRecordsMapper, DeviceRecords> {

    /**
    * 分页
    */
    public Page<DeviceRecords> page(PageParam pageParam, DeviceRecordsParam param) {
        Page<DeviceRecords> mpPage = MpUtil.getMpPage(pageParam, DeviceRecords.class);
        return this.lambdaQuery()
                .select(this.getEntityClass(),MpUtil::excludeBigField)
                .orderByDesc(MpIdEntity::getId)
                .page(mpPage);
    }
    @DS("#dataSource")
    public List<DeviceRecords> selectComputer(String dataSource) {
        return lambdaQuery()
                .in(DeviceRecords::getCategoryId, GoVIewCode.COMPUTER_CATEGORY)
                .list();
    }
}
