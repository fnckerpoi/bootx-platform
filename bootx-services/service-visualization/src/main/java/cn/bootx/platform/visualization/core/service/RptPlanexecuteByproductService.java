package cn.bootx.platform.visualization.core.service;

import cn.bootx.platform.common.core.rest.param.RangeParam;
import cn.bootx.platform.common.core.util.ResultConvertUtil;
import cn.bootx.platform.visualization.core.dao.RptPlanexecuteByproductManager;
import cn.bootx.platform.visualization.core.entity.RptPlanexecuteByproduct;
import cn.bootx.platform.visualization.dto.RptPlanexecuteByproductDto;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.alibaba.excel.util.DateUtils;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 计划完成进度表，关注某产品每批累计生产进度，每个品种产品，以月自然日1-30号生产记录，有排产数量或完工数量的，填入对应的品种日期，无的，填0
 * @author luokanran
 * @since 2023-09-18
 */
@Slf4j
@Service
@RequiredArgsConstructor
@DS("bigScreen")
public class RptPlanexecuteByproductService {
    private final RptPlanexecuteByproductManager rptPlanexecuteByproductManager;

    /**
     * 获取全部
     */
    public List<RptPlanexecuteByproductDto> findAll(){
        return ResultConvertUtil.dtoListConvert(rptPlanexecuteByproductManager.findAll());
    }

    public Object categoryOfEachProduct(RptPlanexecuteByproduct pageParam, RangeParam rangeParam,boolean isHideFprqty) {

        List<RptPlanexecuteByproductDto> srcList = ResultConvertUtil.dtoListConvert(
                rptPlanexecuteByproductManager.lambdaQuery()
                        .between(RptPlanexecuteByproduct::getFbizdatetime,rangeParam.getStartTime(),rangeParam.getEndTime())
                        .orderByAsc(RptPlanexecuteByproduct::getFbizdatetime)
                        .list()
        );

        Map<String,List<RptPlanexecuteByproductDto>> groupByProduct = srcList.stream().collect(Collectors.groupingBy(RptPlanexecuteByproductDto::getFmaterialcode));

        Map<String, TreeMap<String, Object>> result = new HashMap<>();
//        遍历每个产品，生成每个独立的折线表图
        groupByProduct.forEach((k,v)->{
            TreeMap<String, Object> eachTable = new TreeMap<>();
            List<Map<String, Object>> source = v.stream()
                    .map(row -> {
                        Map<String, Object> map = new HashMap<>();
                        map.put("date", DateUtil.format(row.getFbizdatetime(), DateUtils.DATE_FORMAT_10));
                        map.put("计划产量", row.getFapsqtyAccumulation());
                        //当isHideFprqty为true时，过滤掉当天以后的完工数量插入
                        if (isHideFprqty && row.getFbizdatetime().isAfter(LocalDateTime.now())){
                            return map;
                        }
                        map.put("完工数量", row.getFproductqtyAccumulation());
                        return map;
                    }).collect(Collectors.toList());
            eachTable.put("dimensions", source.stream().flatMap(a -> a.keySet().stream()).distinct().collect(Collectors.toList()));
            eachTable.put("source", source);
            result.put(k,eachTable);
        });
        return result;
    }

    public Object yieldOfEachProduct(String targetDate) {
        List<RptPlanexecuteByproductDto> srcList = ResultConvertUtil.dtoListConvert(
                rptPlanexecuteByproductManager.lambdaQuery()
                        .like(RptPlanexecuteByproduct::getFbizdatetime,targetDate)
                        .list());

        Map<String,String> result = new HashMap<>();

        srcList.forEach(a->{
            result.put(a.getFmaterialcode(),"当日产量："+a.getFproductqty());
        });

        return result;
    }
}
