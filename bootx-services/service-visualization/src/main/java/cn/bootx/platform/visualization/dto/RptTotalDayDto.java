package cn.bootx.platform.visualization.dto;

import cn.bootx.platform.common.core.rest.dto.BaseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * MES类-按日统计的数据
 * @author Duriea
 * @date 2023-05-15
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(title = "MES类-按日统计的数据")
@Accessors(chain = true)
public class RptTotalDayDto extends BaseDto {

    @Schema(description = "主键")
    private Long fid;
    @Schema(description = "创建时间")
    private LocalDateTime fcreatedatetime;
    @Schema(description = "创建人；取登录用户")
    private String fcreateman;
    @Schema(description = "最后修改时间")
    private LocalDateTime flastmodifydatetime;
    @Schema(description = "最后修改人；取用户名")
    private String flastmodifyman;
    @Schema(description = "1:广东-生产结束工单总数；2：广东-审核完毕工单总数  3：广东-未开始审核工单数量  4：广东-审核部分完成数量  5:广东-无需审核数量")
    private Integer ftypeid;
    @Schema(description = "日期")
    private LocalDateTime fdate;
    @Schema(description = "数量或比例值")
    private BigDecimal fqty;
    @Schema(description = "指标数据有特殊情况时，在此栏说明")
    private String fmemo;
    @Schema(description = "区域类别：  0：中心  1：基地   2：厂级  3：车间  4：产线")
    private Integer fareatype;
    @Schema(description = "区域id。 区域类别： 中心：此值无效 基地：关联t_org_company:fid 厂级：关联t_org_factory:fid 车间级：关联t_org_workshop:fid 产线级：关联t_org_productline:fid")
    private Long fareaid;

}
