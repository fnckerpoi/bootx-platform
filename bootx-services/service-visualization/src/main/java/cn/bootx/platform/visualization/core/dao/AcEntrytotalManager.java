package cn.bootx.platform.visualization.core.dao;

import cn.bootx.platform.common.mybatisplus.impl.BaseManager;
import cn.bootx.platform.visualization.core.dao.MapperImpl.AcEntrytotalMapper;
import cn.bootx.platform.visualization.param.AcEntrytotalParam;
import cn.bootx.platform.visualization.core.entity.AcEntrytotal;
import cn.bootx.platform.common.core.rest.param.PageParam;
import cn.bootx.platform.common.mybatisplus.util.MpUtil;
import cn.bootx.platform.common.query.generator.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 考勤记录
 * @author Duriea
 * @since 2023-08-15
 */
@Repository
@RequiredArgsConstructor
public class AcEntrytotalManager extends BaseManager<AcEntrytotalMapper, AcEntrytotal> {

    /**
    * 分页
    */
    public Page<AcEntrytotal> page(PageParam pageParam, AcEntrytotalParam param) {
        Page<AcEntrytotal> mpPage = MpUtil.getMpPage(pageParam, AcEntrytotal.class);
        QueryWrapper<AcEntrytotal> wrapper = QueryGenerator.generator(param, this.getEntityClass());
        wrapper.select(this.getEntityClass(),MpUtil::excludeBigField)
                .orderByDesc(MpUtil.getColumnName(AcEntrytotal::getFid));
        return this.page(mpPage,wrapper);
    }

    public List<AcEntrytotal> getLastRecord() {
        return baseMapper.getLastRecord();
    }
}
