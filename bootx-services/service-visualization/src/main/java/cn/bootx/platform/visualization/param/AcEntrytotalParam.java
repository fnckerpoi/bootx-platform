package cn.bootx.platform.visualization.param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalDate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 考勤记录
 * @author Duriea
 * @since 2023-08-15
 */
@Data
@Schema(title = "考勤记录")
@Accessors(chain = true)
public class AcEntrytotalParam {

    @Schema(description= "主键")
    private Long id;

    @Schema(description = "主键")
    private Long fid;
    @Schema(description = "创建时间")
    private LocalDateTime fcreatedatetime;
    @Schema(description = "创建人；取登录用户")
    private String fcreateman;
    @Schema(description = "最后修改时间")
    private LocalDateTime flastmodifydatetime;
    @Schema(description = "最后修改人；取用户名")
    private String flastmodifyman;
    @Schema(description = "")
    private LocalDateTime fbizdatetime;
    @Schema(description = "")
    private String fnumber;
    @Schema(description = "区域标识，与考勤系统wmmj库的tblLink表关联")
    private String flinkid;
    @Schema(description = "区域名称")
    private String flinkname;
    @Schema(description = "门禁标识，与考勤系统wmmj库的Ctrller表关联")
    private Integer fctrlid;
    @Schema(description = "门禁名称")
    private String fctrlname;
    @Schema(description = "所属车间，关联t_org_workshop")
    private Long fworkshopid;
    @Schema(description = "所属车间全路径名")
    private String fworkshopfullname;
    @Schema(description = "开门次数")
    private Integer fentrytotal;
    @Schema(description = "")
    private BigDecimal fx;
    @Schema(description = "")
    private BigDecimal fy;

}
