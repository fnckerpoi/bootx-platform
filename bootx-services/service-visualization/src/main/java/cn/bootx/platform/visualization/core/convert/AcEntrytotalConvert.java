package cn.bootx.platform.visualization.core.convert;


import cn.bootx.platform.visualization.core.entity.AcEntrytotal;
import cn.bootx.platform.visualization.dto.AcEntrytotalDto;
import cn.bootx.platform.visualization.param.AcEntrytotalParam;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 考勤记录
 * @author Duriea
 * @since 2023-08-15
 */
@Mapper
public interface AcEntrytotalConvert {
    AcEntrytotalConvert CONVERT = Mappers.getMapper(AcEntrytotalConvert.class);

    AcEntrytotal convert(AcEntrytotalParam in);

    AcEntrytotalDto convert(AcEntrytotal in);

}
