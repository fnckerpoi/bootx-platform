package cn.bootx.platform.visualization.core.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 记录每条产线每天最先开机时间(TRcdProductionlineFirstBoot)表实体类
 *
 * @author makejava
 * @since 2024-01-30 16:08:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("t_rcd_productionline_first_boot")
public class RcdProductionlineFirstBoot {
    //主键
    @TableId(type = IdType.INPUT)
    private Long fid;
//创建时间
    private Date fcreatedatetime;
//创建人；取登录用户
    private String fcreateman;
//最后修改时间
    private Date flastmodifydatetime;
//最后修改人；取用户名
    private String flastmodifyman;
//业务日期
    private Date fdate;
//产线编号
    private String fproductionlinecode;
//首次开机时间
    private Date ffirstbootdatetime;

}

