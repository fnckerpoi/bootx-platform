package cn.bootx.platform.visualization.dto;

import cn.bootx.platform.common.core.rest.dto.BaseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 
 * @author xxm
 * @date 2023-05-18
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(title = "")
@Accessors(chain = true)
public class DeviceRecordsDto extends BaseDto {

    @Schema(description = "")
    private String name;
    @Schema(description = "")
    private String description;
    @Schema(description = "")
    private Integer categoryId;
    @Schema(description = "")
    private Integer vendorId;
    @Schema(description = "")
    private String mac;
    @Schema(description = "")
    private String ip;
    @Schema(description = "")
    private String photo;
    @Schema(description = "")
    private Double price;
    @Schema(description = "")
    private LocalDate purchased;
    @Schema(description = "")
    private LocalDate expired;
    @Schema(description = "")
    private Integer depreciationRuleId;
    @Schema(description = "")
    private String assetNumber;
    @Schema(description = "")
    private LocalDateTime deletedAt;
    @Schema(description = "")
    private LocalDateTime createdAt;
    @Schema(description = "")
    private LocalDateTime updatedAt;
    @Schema(description = "")
    private LocalDateTime discardAt;

}
