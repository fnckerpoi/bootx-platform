package cn.bootx.platform.visualization.core.dao;

import cn.bootx.platform.common.mybatisplus.impl.BaseManager;
import cn.bootx.platform.visualization.core.dao.MapperImpl.TRcdRunminuteMapper;
import cn.bootx.platform.visualization.core.entity.TRcdRunminute;
import cn.bootx.platform.visualization.param.bizParam.GetProductionLineRunMinuteReportParam;
import cn.bootx.platform.visualization.vo.ProductionLineRunMinuteVO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RcdRunminuteManager extends BaseManager<TRcdRunminuteMapper, TRcdRunminute> {
    public List<ProductionLineRunMinuteVO> getProductionLineRunMinuteReport(GetProductionLineRunMinuteReportParam param) {
        return baseMapper.getProductionLineRunMinuteReport(param);
    }
}
