package cn.bootx.platform.visualization.core.dao.MapperImpl;

import cn.bootx.platform.visualization.core.entity.RptPlanexecuteByproduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 计划完成进度表，关注某产品每批累计生产进度，每个品种产品，以月自然日1-30号生产记录，有排产数量或完工数量的，填入对应的品种日期，无的，填0
 * @author luokanran
 * @since 2023-09-18
 */
@Mapper
public interface RptPlanexecuteByproductMapper extends BaseMapper<RptPlanexecuteByproduct> {
}
