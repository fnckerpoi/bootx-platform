package cn.bootx.platform.visualization.echarts;

import cn.hutool.core.map.MapUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.Predicate;

public class EchartsDataSet<T> {
    private List<String> dimensions;

    private List<T> source;

    private Map<String,Object> payload;

    private EchartsDataSet() {
    }

    public static <T> EchartsDataSet<T> create(){
        EchartsDataSet<T> dataSet = new EchartsDataSet<>();
        dataSet.dimensions = new ArrayList<>();
        dataSet.payload = new HashMap<>();
        dataSet.source = new ArrayList<>();
        return dataSet;
    }

    public void clearDimension(){
        this.dimensions.clear();
    }

    public void clearSource(){
        this.source.clear();
    }
    public void clearPayload(){
        this.payload.clear();
    }

    public boolean addDimension(String dimension){
        return this.dimensions.add(dimension);
    }

    public boolean removeDimensionByContent(String dimension){
        return this.dimensions.removeIf(i -> i.equals(dimension));
    }

    public boolean addSource(T t){
        return this.source.add(t);
    }

    public boolean removeSourceByCondition(Predicate<T> predicate){
        return this.source.removeIf(predicate);
    }

    public void putPayload(String k,Object v){
        this.payload.put(k, v);
    }

    public void putPayloadAll(Map<String,Object> map){
        this.payload.putAll(map);
    }

    public void removePayloadByKey(String key){
        this.payload.remove(key);
    }

    public Map<String,Object> toMap(){
        return this.toMap(null);
    }

    public Map<String,Object> toMap(String payloadKey){
        Map<String, Object> map = new HashMap<>();

        map.put("dimensions",this.dimensions);
        map.put("source",this.source);

        if (MapUtil.isNotEmpty(this.payload)) {
            if (StringUtils.isBlank(payloadKey)){
                map.putAll(this.payload);
            }else {
                map.put(payloadKey,payload);
            }
        }

        return map;
    }

}
