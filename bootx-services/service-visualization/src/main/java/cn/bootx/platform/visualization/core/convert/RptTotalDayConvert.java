package cn.bootx.platform.visualization.core.convert;

import cn.bootx.platform.visualization.core.entity.RptTotalDay;
import cn.bootx.platform.visualization.dto.RptTotalDayDto;
import cn.bootx.platform.visualization.param.RptTotalDayParam;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * MES类-按日统计的数据
 * @author Duriea
 * @date 2023-05-15
 */
@Mapper
public interface RptTotalDayConvert {
    RptTotalDayConvert CONVERT = Mappers.getMapper(RptTotalDayConvert.class);

    RptTotalDay convert(RptTotalDayParam in);

    RptTotalDayDto convert(RptTotalDay in);

}