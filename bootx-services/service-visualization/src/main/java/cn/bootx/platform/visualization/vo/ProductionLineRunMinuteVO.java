package cn.bootx.platform.visualization.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProductionLineRunMinuteVO {
    // 产线编号
    private String fproductioncode;

    // 产线名称
    private String productionlineName;

    // 车间名称
    private String workshopName;

    // 工厂名称
    private String factoryName;

    // 公司名称
    private String companyName;

    // 运行时间字符串
    private String runMinuteStr;
}
