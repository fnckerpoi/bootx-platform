package cn.bootx.platform.visualization.core.entity;

import java.time.LocalDateTime;

import cn.bootx.platform.common.core.function.EntityBaseFunction;
import cn.bootx.platform.common.mybatisplus.base.MpIdEntity;

import cn.bootx.platform.visualization.core.convert.DeviceTracksConvert;
import cn.bootx.platform.visualization.dto.DeviceTracksDto;
import cn.bootx.platform.visualization.param.DeviceTracksParam;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
* 
* @author Duriea
* @date 2023-05-19
*/
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@TableName("device_tracks")
public class DeviceTracks extends MpIdEntity implements EntityBaseFunction<DeviceTracksDto> {

    /**  */
    private Long deviceId;
    /**  */
    private Integer userId;
    /**  */
    private LocalDateTime lendTime;
    /**  */
    private String lendDescription;
    /**  */
    private LocalDateTime planReturnTime;
    /**  */
    private LocalDateTime returnTime;
    /**  */
    private String returnDescription;
    /**  */
    private LocalDateTime deletedAt;
    /**  */
    private LocalDateTime createdAt;
    /**  */
    private LocalDateTime updatedAt;

    /** 创建对象 */
    public static DeviceTracks init(DeviceTracksParam in) {
            return DeviceTracksConvert.CONVERT.convert(in);
    }

    /** 转换成dto */
    @Override
    public DeviceTracksDto toDto() {
        return DeviceTracksConvert.CONVERT.convert(this);
    }
}
